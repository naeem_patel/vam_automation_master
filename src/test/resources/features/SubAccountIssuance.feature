@CS-SubAccountIssuance
Feature: Sub Account Issuance Request and Maintenance

  #CREATE -> SUMBIT -> DISCARD
  @CS-CSCRUD04
  Scenario Outline: Create and Discard Sub Account Issuance Request
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>" and save it
    Then I verify Sub Account Issuance Request is in "Draft" status
    Then I discard the record and verify

    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_001                    |

  #CREATE -> SUMBIT -> REJECT
  @CS-CSCRUD05
  Scenario Outline: Create and Reject Sub Account Issuance Request
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>"
    Then I verify Sub Account Issuance Request is in "Submitted" status
    And I switch to "Bob" User and "HKVAM" entity
    And I reject the Sub Account Issuance Request
    Then I verify Sub Account Issuance Request is in "Rejected" status
    Then I discard the record and verify

    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_001                    |

  #CREATE -> SUBMIT -> APPROVE -> VIEW
  @CS-CSCRUD06
  Scenario Outline: Create and Approve Sub Account Issuance Request and View it in Maintenance screen
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>"
    Then I verify Sub Account Issuance Request is in "Submitted" status
    And I switch to "Bob" User and "HKVAM" entity
    And I approve the Sub Account Issuance Request
    Then I verify Sub Account Issuance Request is in "Approved" status
    Then I navigate to Sub Account Maintenance Page
    Then I verify Sub Account is in "Approved" status for "<Sub Account Issuance Request>"

    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_001                    |

  #CREATE -> SUBMIT -> APPROVE -> VIEW -> MODIFY -> APPROVE
  @CS-CSCRUD07
  Scenario Outline: Create and Approve Sub Account Issuance Request and View it in Maintenance screen
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>"
    And I switch to "Bob" User and "HKVAM" entity
    And I approve the Sub Account Issuance Request
    Then I navigate to Sub Account Maintenance Page
    And I modify Virtual Account and Sumbit it
    Then I verify Sub Account Issuance Request is in "Submitted" status
    And I switch to "Alice" User and "HKVAM" entity
    And I approve the Sub Account Issuance Request
    Then I verify Sub Account Issuance Request is in "Approved" status

    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_001                    |

  #FieldValidation -> Customer Field
  @CS-CSFV06
  Scenario Outline: Verify the validation messages on SubAccount Issuance Request Screen on Customer Field
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>" to verify validation messages
    Then I verify field validations for Customer field in SubAccount Issuance Request

    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_003                    |

  #FieldValidation -> Physical Account Field
  @CS-CSFV07
  Scenario Outline: Verify the validation messages on SubAccount Issuance Request Screen on Physical Account Field
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>" to verify validation messages
    Then I verify field validations for Physical Account field in SubAccount Issuance Request

    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_003                    |

  #FieldValidation -> VA Structure Field
  @CS-CSFV08
  Scenario Outline: Verify the validation messages on SubAccount Issuance Request Screen on VA Structure Field
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>" to verify validation messages
    Then I verify field validations for VA Structure field in SubAccount Issuance Request

    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_003                    |

  #FieldValidation -> SubAccount Number Field
  @CS-CSFV09
  Scenario Outline: Verify the validation messages on SubAccount Issuance Request Screen on SubAccount Number and No.of Accounts Field
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>" to verify validation messages
    Then I verify field validations for SubAccount Number field in SubAccount Issuance Request

    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_001                    |
      | TC_VA_003                    |

  #FieldValidation -> Account Owner Name Field
  @CS-CSFV10
  Scenario Outline: Verify the validation messages on SubAccount Issuance Request Screen on Account Owner Name Field
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>" to verify validation messages
    Then I verify field validations for Account Owner Name field in SubAccount Issuance Request

    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_003                    |

  #FieldValidation -> Phone Number Field
  @CS-CSFV11
  Scenario Outline: Verify the validation messages on SubAccount Issuance Request Screen on Account Owner Name Field
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>" to verify validation messages
    Then I verify field validations for Phone Number field in SubAccount Issuance Request

    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_003                    |

  #FieldValidation -> Email ID Field
  @CS-CSFV12
  Scenario Outline: Verify the validation messages on SubAccount Issuance Request Screen on EmailID Field
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>" to verify validation messages
    Then I verify field validations for EmailID field in SubAccount Issuance Request

    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_003                    |

  #FieldValidation -> PinCode Field
  @CS-CSFV13
  Scenario Outline: Verify the validation messages on SubAccount Issuance Request Screen on PinCode Field
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>" to verify validation messages
    Then I verify field validations for PinCode field in SubAccount Issuance Request

    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_003                    |

  #FieldValidation -> Address1 Field
  @CS-CSFV14
  Scenario Outline: Verify the validation messages on SubAccount Issuance Request Screen on Address1 Field
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>" to verify validation messages
    Then I verify field validations for Address1 field in SubAccount Issuance Request

    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_003                    |

  #FieldValidation -> Address2 Field
  @CS-CSFV15
  Scenario Outline: Verify the validation messages on SubAccount Issuance Request Screen on Address2 Field
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>" to verify validation messages
    Then I verify field validations for Address2 field in SubAccount Issuance Request

    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_003                    |

  #FieldValidation -> Address3 Field
  @CS-CSFV16
  Scenario Outline: Verify the validation messages on SubAccount Issuance Request Screen on Address3 Field
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>" to verify validation messages
    Then I verify field validations for Address3 field in SubAccount Issuance Request

    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_003                    |

  #FieldValidation -> Address4 Field
  @CS-CSFV17
  Scenario Outline: Verify the validation messages on SubAccount Issuance Request Screen on Address4 Field
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>" to verify validation messages
    Then I verify field validations for Address4 field in SubAccount Issuance Request

    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_003                    |