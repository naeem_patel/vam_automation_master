@VirtualAccount
Feature: Virtual Account Feature

  @GTBIPSH-CS512
  Scenario Outline: Create and Approve Sub Account Prefix Configurations
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Prefix Page
    When I add Sub Account Prefix for "<Sub Account Prefix>"
    Then I verify Sub Account Prefix is in "Submitted" status
    And I switch to "Bob" User and "HKVAM" entity
    And I approve the Sub Account Prefix
    Then I verify Sub Account Prefix is in "Approved" status
    Examples: 
      | Sub Account Prefix |
      | TC_VA_001|
      

   @GTBIPSH-CS513
  Scenario Outline: Create and Approve Sub Account Issuance Request
  	Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>"
    Then I verify Sub Account Issuance Request is in "Submitted" status
    And I switch to "Bob" User and "HKVAM" entity
    And I approve the Sub Account Issuance Request
    Then I verify Sub Account Issuance Request is in "Approved" status
    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_001|
    
      
    @GTBIPSH-CS514
  Scenario Outline: Verify the Created Virtual Account
  	Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    Then I am on Sub Account Maintenance Page
    Then I verify Sub Account is in "Approved" status for "<Sub Account Maintenance>"
    Examples: 
      | Sub Account Maintenance |
      | TC_VA_001|

      
     
     @GTBIPSH-CS515
  Scenario Outline: Create and Approve Context Id Issuance Request
  	Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Context ID Issuance Page
    When I add Context ID Issuance Request for "<ContextID Issuance Request>"
    Then I verify Context ID Issuance Request is in "Submitted" status
    And I switch to "Bob" User and "HKVAM" entity
    And I approve the Context ID Issuance Request
    Then I verify Context ID Issuance Request is in "Approved" status
     Examples: 
      | ContextID Issuance Request|
      | TC_VA_002|
      | TC_VA_003|
      | TC_VA_004|
      
      @GTBIPSH-CS516
  Scenario Outline: Verify the Created Context ID
  	Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    Then I am on Context ID Maintenance Page
    Then I verify Context ID is in "Approved" status for "<Context ID Maintenance>"
    Examples: 
      | Context ID Maintenance |
      | TC_VA_002|
      | TC_VA_003|
      | TC_VA_004|
      
      
 #------------------------------------------------SAVE DISCARD REJECT MODIFY APPROVE FLOW------------------------------------# 
  
  
  #CREATE -> SUMBIT -> DISCARD
   @CS-CSCRUD01
  Scenario Outline: Create and Discard Sub Account Prefix Configuration
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Prefix Page
    When I add Sub Account Prefix for "<Sub Account Prefix>" and save it
    Then I verify Sub Account Prefix is in "Draft" status
    Then I discard the record and verify
    Examples: 
      | Sub Account Prefix |
      | TC_VA_001|
      
  #CREATE -> SUMBIT -> APPROVE    
  @CS-CSCRUD02
  Scenario Outline: Create and Approve Sub Account Prefix Configuration
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Prefix Page
    When I add Sub Account Prefix for "<Sub Account Prefix>"
    Then I verify Sub Account Prefix is in "Submitted" status
    And I switch to "Bob" User and "HKVAM" entity
    And I approve the Sub Account Prefix
    Then I verify Sub Account Prefix is in "Approved" status
    Examples: 
      | Sub Account Prefix |
      | TC_VA_001|
      
      
    #CREATE -> SUMBIT -> REJECT
    @CS-CSCRUD031
  Scenario Outline: Create and Reject Sub Account Prefix Configuration
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Prefix Page
    When I add Sub Account Prefix for "<Sub Account Prefix>"
    Then I verify Sub Account Prefix is in "Submitted" status
    And I switch to "Bob" User and "HKVAM" entity
    And I reject the Sub Account Prefix
    Then I verify Sub Account Prefix is in "Rejected" status
    Examples: 
      | Sub Account Prefix |
      | TC_VA_001|
  
 #-------------------------Field Validation ----------------------------------------------#
   
   @CS-CSFV01		@CS-FieldValidation
   Scenario Outline: Verify the validation messages on SubAccount Prefix Screen on Customer Field
   Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Prefix Page
    When I add Sub Account Prefix for "<Sub Account Prefix>" to verify validation messages
		Then I verify field validations for Customer field in SubAccount Prefix
    Examples: 
      | Sub Account Prefix |
      | TC_VA_003|
      
      
    @CS-CSFV02	@CS-FieldValidation
   Scenario Outline: Verify the validation messages on SubAccount Prefix Screen on Physical Account Field
   Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Prefix Page
    When I add Sub Account Prefix for "<Sub Account Prefix>" to verify validation messages
		Then I verify field validations for Physical Account field in SubAccount Prefix
    Examples: 
      | Sub Account Prefix |
      | TC_VA_003|
      
    
       @CS-CSFV03	@CS-FieldValidation
   Scenario Outline: Verify the validation messages on SubAccount Prefix Screen on Product Field
   Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Prefix Page
    When I add Sub Account Prefix for "<Sub Account Prefix>" to verify validation messages
		Then I verify field validations for Product field in SubAccount Prefix
    Examples: 
      | Sub Account Prefix |
      | TC_VA_003|
      
      
       @CS-CSFV04	@CS-FieldValidation
   Scenario Outline: Verify the validation messages on SubAccount Prefix Screen on VA Structure Field
   Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Prefix Page
    When I add Sub Account Prefix for "<Sub Account Prefix>" to verify validation messages
		Then I verify field validations for VA Structure field in SubAccount Prefix 
    Examples: 
      | Sub Account Prefix |
      | TC_VA_003|
      
      @CS-CSFV05	@CS-FieldValidation
   Scenario Outline: Verify the validation messages on SubAccount Prefix Screen on Context ID Length Field
   Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Prefix Page
    When I add Sub Account Prefix for "<Sub Account Prefix>" to verify validation messages
		Then I verify field validations for Context ID Length field in SubAccount Prefix 
    Examples: 
      | Sub Account Prefix |
      | TC_VA_003|
      
      
    @CS-CSFV06	@CS-FieldValidation
   Scenario Outline: Verify the validation messages on SubAccount Issuance Request Screen on Customer Field
   Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>" to verify validation messages
		Then I verify field validations for Customer field in SubAccount Issuance Request
    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_003|
      
    @CS-CSFV07	@CS-FieldValidation
   Scenario Outline: Verify the validation messages on SubAccount Issuance Request Screen on Physical Account Field
   Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>" to verify validation messages
		Then I verify field validations for Physical Account field in SubAccount Issuance Request
    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_003|
      
      
    @CS-CSFV08	@CS-FieldValidation
   Scenario Outline: Verify the validation messages on SubAccount Issuance Request Screen on VA Structure Field
   Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>" to verify validation messages
		Then I verify field validations for VA Structure field in SubAccount Issuance Request
    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_003|
      
      
     
    @CS-CSFV09	@CS-FieldValidation
   Scenario Outline: Verify the validation messages on SubAccount Issuance Request Screen on SubAccount Number and No.of Accounts Field
   Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>" to verify validation messages
		Then I verify field validations for SubAccount Number field in SubAccount Issuance Request
    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_001|
      | TC_VA_003|
      
      
       @CS-CSFV10	@CS-FieldValidation
   Scenario Outline: Verify the validation messages on SubAccount Issuance Request Screen on Account Owner Name Field
   Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>" to verify validation messages
		Then I verify field validations for Account Owner Name field in SubAccount Issuance Request
    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_003|
      
      
      
    @CS-CSFV11	@CS-FieldValidation
   Scenario Outline: Verify the validation messages on SubAccount Issuance Request Screen on Account Owner Name Field
   Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>" to verify validation messages
		Then I verify field validations for Phone Number field in SubAccount Issuance Request
    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_003|
      
      
    
    @CS-CSFV12	@CS-FieldValidation
   Scenario Outline: Verify the validation messages on SubAccount Issuance Request Screen on EmailID Field
   Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>" to verify validation messages
		Then I verify field validations for EmailID field in SubAccount Issuance Request
    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_003|
      
      
    @CS-CSFV13	@CS-FieldValidation
   Scenario Outline: Verify the validation messages on SubAccount Issuance Request Screen on PinCode Field
   Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>" to verify validation messages
		Then I verify field validations for PinCode field in SubAccount Issuance Request
    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_003|
      
      
      @CS-CSFV14
   Scenario Outline: Verify the validation messages on SubAccount Issuance Request Screen on Address1 Field
   Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>" to verify validation messages
		Then I verify field validations for Address1 field in SubAccount Issuance Request
    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_003|
   
      
   @CS-CSFV15
   Scenario Outline: Verify the validation messages on SubAccount Issuance Request Screen on Address2 Field
   Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>" to verify validation messages
		Then I verify field validations for Address2 field in SubAccount Issuance Request
    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_003|
      
      
       @CS-CSFV16
   Scenario Outline: Verify the validation messages on SubAccount Issuance Request Screen on Address3 Field
   Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>" to verify validation messages
		Then I verify field validations for Address3 field in SubAccount Issuance Request
    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_003|
      
      
      
      @CS-CSFV17
   Scenario Outline: Verify the validation messages on SubAccount Issuance Request Screen on Address4 Field
   Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Issuance Request Page
    When I add Sub Account Issuance Request for "<Sub Account Issuance Request>" to verify validation messages
		Then I verify field validations for Address4 field in SubAccount Issuance Request
    Examples: 
      | Sub Account Issuance Request |
      | TC_VA_003|
      