@CS-SubAccountPrefixConfig
Feature: Sub Account Prefix Configuration

  #CREATE -> SUMBIT -> DISCARD
  @CS-CSCRUD01
  Scenario Outline: Create and Discard Sub Account Prefix Configuration
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Prefix Page
    When I add Sub Account Prefix for "<Sub Account Prefix>" and save it
    Then I verify Sub Account Prefix is in "Draft" status
    Then I discard the record and verify

    Examples: 
      | Sub Account Prefix |
      | TC_VA_001          |

  #CREATE -> SUMBIT -> REJECT
  @CS-CSCRUD02
  Scenario Outline: Create and Reject Sub Account Prefix Configuration
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Prefix Page
    When I add Sub Account Prefix for "<Sub Account Prefix>"
    Then I verify Sub Account Prefix is in "Submitted" status
    And I switch to "Bob" User and "HKVAM" entity
    And I reject the Sub Account Prefix
    Then I verify Sub Account Prefix is in "Rejected" status
    Then I discard the record and verify

    Examples: 
      | Sub Account Prefix |
      | TC_VA_001          |

  #CREATE -> SUMBIT -> APPROVE
  @CS-CSCRUD03
  Scenario Outline: Create and Approve Sub Account Prefix Configuration
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Prefix Page
    When I add Sub Account Prefix for "<Sub Account Prefix>"
    Then I verify Sub Account Prefix is in "Submitted" status
    And I switch to "Bob" User and "HKVAM" entity
    And I approve the Sub Account Prefix
    Then I verify Sub Account Prefix is in "Approved" status

    Examples: 
      | Sub Account Prefix |
      | TC_VA_001          |

  #FieldValidation -> Customer Field
  @CS-CSFV01
  Scenario Outline: Verify the validation messages on SubAccount Prefix Screen on Customer Field
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Prefix Page
    When I add Sub Account Prefix for "<Sub Account Prefix>" to verify validation messages
    Then I verify field validations for Customer field in SubAccount Prefix

    Examples: 
      | Sub Account Prefix |
      | TC_VA_003          |

  #FieldValidation -> Physical Account Field
  @CS-CSFV02
  Scenario Outline: Verify the validation messages on SubAccount Prefix Screen on Physical Account Field
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Prefix Page
    When I add Sub Account Prefix for "<Sub Account Prefix>" to verify validation messages
    Then I verify field validations for Physical Account field in SubAccount Prefix

    Examples: 
      | Sub Account Prefix |
      | TC_VA_003          |

  #FieldValidation -> Product Field
  @CS-CSFV03
  Scenario Outline: Verify the validation messages on SubAccount Prefix Screen on Product Field
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Prefix Page
    When I add Sub Account Prefix for "<Sub Account Prefix>" to verify validation messages
    Then I verify field validations for Product field in SubAccount Prefix

    Examples: 
      | Sub Account Prefix |
      | TC_VA_003          |

  #FieldValidation -> VA Structure Field
  @CS-CSFV04
  Scenario Outline: Verify the validation messages on SubAccount Prefix Screen on VA Structure Field
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Prefix Page
    When I add Sub Account Prefix for "<Sub Account Prefix>" to verify validation messages
    Then I verify field validations for VA Structure field in SubAccount Prefix

    Examples: 
      | Sub Account Prefix |
      | TC_VA_003          |

  #FieldValidation -> Context ID Length Field
  @CS-CSFV05
  Scenario Outline: Verify the validation messages on SubAccount Prefix Screen on Context ID Length Field
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Prefix Page
    When I add Sub Account Prefix for "<Sub Account Prefix>" to verify validation messages
    Then I verify field validations for Context ID Length field in SubAccount Prefix

    Examples: 
      | Sub Account Prefix |
      | TC_VA_003          |

  #CREATE -> SUMBIT -> APPROVE
  @CS-CSSCEN01
  Scenario Outline: Create Sub Account Prefix with SubAccount Prefix is Auto and Context is Not Applicable
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Prefix Page
    When I add Sub Account Prefix for "<Sub Account Prefix>"
    Then I verify Sub Account Prefix is in "Submitted" status
    And I switch to "Bob" User and "HKVAM" entity
    And I approve the Sub Account Prefix
    Then I verify Sub Account Prefix is in "Approved" status

    Examples: 
      | Sub Account Prefix |
      | TC_VA_001          |

  #CREATE -> SUMBIT -> APPROVE
  @CS-CSSCEN02
  Scenario Outline: Create Sub Account Prefix with SubAccount Prefix is Auto and Context is Auto
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Prefix Page
    When I add Sub Account Prefix for "<Sub Account Prefix>"
    Then I verify Sub Account Prefix is in "Submitted" status
    And I switch to "Bob" User and "HKVAM" entity
    And I approve the Sub Account Prefix
    Then I verify Sub Account Prefix is in "Approved" status

    Examples: 
      | Sub Account Prefix |
      | TC_VA_002          |

  #CREATE -> SUMBIT -> APPROVE
  @CS-CSSCEN03
  Scenario Outline: Create Sub Account Prefix with SubAccount Prefix is Manual and Context is Auto
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Prefix Page
    When I add Sub Account Prefix for "<Sub Account Prefix>"
    Then I verify Sub Account Prefix is in "Submitted" status
    And I switch to "Bob" User and "HKVAM" entity
    And I approve the Sub Account Prefix
    Then I verify Sub Account Prefix is in "Approved" status

    Examples: 
      | Sub Account Prefix |
      | TC_VA_003          |

  #CREATE -> SUMBIT -> APPROVE
  @CS-CSSCEN04
  Scenario Outline: Create Sub Account Prefix with SubAccount Prefix is Manual and Context is Manual
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Prefix Page
    When I add Sub Account Prefix for "<Sub Account Prefix>"
    Then I verify Sub Account Prefix is in "Submitted" status
    And I switch to "Bob" User and "HKVAM" entity
    And I approve the Sub Account Prefix
    Then I verify Sub Account Prefix is in "Approved" status

    Examples: 
      | Sub Account Prefix |
      | TC_VA_004          |

  #CREATE -> SUMBIT -> APPROVE
  @CS-CSSCEN05
  Scenario Outline: Create Sub Account Prefix with SubAccount Prefix Manual and Context is Not Applicable
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Sub Account Prefix Page
    When I add Sub Account Prefix for "<Sub Account Prefix>"
    Then I verify Sub Account Prefix is in "Submitted" status
    And I switch to "Bob" User and "HKVAM" entity
    And I approve the Sub Account Prefix
    Then I verify Sub Account Prefix is in "Approved" status

    Examples: 
      | Sub Account Prefix |
      | TC_VA_005          |
