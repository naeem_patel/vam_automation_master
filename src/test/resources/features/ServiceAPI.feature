@ServiceAPIsVirtualAccount
Feature: Service API Virtual Account Feature

  @CS-SAPI01
  Scenario Outline: Trigger GET request for AccountClosureGetEntities
    Given I trigger GET request for AccountClosureGetEntities with parameters "<Parameters>"
    Then I verify the response status is "Success"

    Examples: 
      | Parameters |
      | TC_VA_001  |

  @CS-SAPI02
  Scenario Outline: Trigger GET request for AccountTransferGetEntities
    Given I trigger GET request for AccountTransferGetEntities with parameters "<Parameters>"
    Then I verify the response status is "Success"

    Examples: 
      | Parameters |
      | TC_VA_002  |

  @CS-SAPI03
  Scenario Outline: Trigger GET request for AllocationSummaryGetEntities
    Given I trigger GET request for AllocationSummaryGetEntities with parameters "<Parameters>"
    Then I verify the response status is "Success"

    Examples: 
      | Parameters |
      | TC_VA_003  |

  @CS-SAPI04
  Scenario Outline: Trigger GET request for BankConfigurationGetEntities
    Given I trigger GET request for BankConfigurationGetEntities with parameters "<Parameters>"
    Then I verify the response status is "Success"

    Examples: 
      | Parameters |
      | TC_VA_004  |

  @CS-SAPI05
  Scenario Outline: Trigger GET request for CustomerInterestBVLGetEntities
    Given I trigger GET request for CustomerInterestBVLGetEntities with parameters "<Parameters>"
    Then I verify the response status is "Success"

    Examples: 
      | Parameters |
      | TC_VA_005  |

  @CS-SAPI06
  Scenario Outline: Trigger GET request for BVLProfileLinkageGetEntities
    Given I trigger GET request for BVLProfileLinkageGetEntities with parameters "<Parameters>"
    Then I verify the response status is "Success"

    Examples: 
      | Parameters |
      | TC_VA_006  |

  @CS-SAPI07
  Scenario Outline: Trigger GET request for ContextIDIssuGetEntities
    Given I trigger GET request for ContextIDIssuGetEntities with parameters "<Parameters>"
    Then I verify the response status is "Success"

    Examples: 
      | Parameters |
      | TC_VA_007  |

  @CS-SAPI08
  Scenario Outline: Trigger GET request for ContextIDRequestGetEntities
    Given I trigger GET request for ContextIDRequestGetEntities with parameters "<Parameters>"
    Then I verify the response status is "Success"

    Examples: 
      | Parameters |
      | TC_VA_008  |

  @CS-SAPI09
  Scenario Outline: Trigger GET request for CustomerIntProfileGetEntities
    Given I trigger GET request for CustomerIntProfileGetEntities with parameters "<Parameters>"
    Then I verify the response status is "Success"

    Examples: 
      | Parameters |
      | TC_VA_009  |

  @CS-SAPI10
  Scenario Outline: Trigger GET request for DashboardGetIssuanceMetric
    Given I trigger GET request for DashboardGetIssuanceMetric with parameters "<Parameters>"
    Then I verify the response status is "Success"

    Examples: 
      | Parameters |
      | TC_VA_010  |

  @CS-SAPI11
  Scenario Outline: Trigger GET request for DashboardGetPhysicalAcct
    Given I trigger GET request for DashboardGetPhysicalAcct with parameters "<Parameters>"
    Then I verify the response status is "Success"

    Examples: 
      | Parameters |
      | TC_VA_011  |

  @CS-SAPI12
  Scenario Outline: Trigger GET request for DashboardGetVirtualAcct
    Given I trigger GET request for DashboardGetVirtualAcct with parameters "<Parameters>"
    Then I verify the response status is "Success"

    Examples: 
      | Parameters |
      | TC_VA_012  |

  @CS-SAPI13
  Scenario Outline: Trigger GET request for ProductConfigGetEntities
    Given I trigger GET request for ProductConfigGetEntities with parameters "<Parameters>"
    Then I verify the response status is "Success"

    Examples: 
      | Parameters |
      | TC_VA_013  |

  @CS-SAPI14
  Scenario Outline: Trigger GET request for SubAcctIssuanceGetEntities
    Given I trigger GET request for SubAcctIssuanceGetEntities with parameters "<Parameters>"
    Then I verify the response status is "Success"

    Examples: 
      | Parameters |
      | TC_VA_014  |

  @CS-SAPI15
  Scenario Outline: Trigger GET request for SubAcctIssuanceReqGetEntities
    Given I trigger GET request for SubAcctIssuanceReqGetEntities with parameters "<Parameters>"
    Then I verify the response status is "Success"

    Examples: 
      | Parameters |
      | TC_VA_015  |

  @CS-SAPI16
  Scenario Outline: Trigger GET request for SubAcctPrefixConfigGetEntities
    Given I trigger GET request for SubAcctPrefixConfigGetEntities with parameters "<Parameters>"
    Then I verify the response status is "Success"

    Examples: 
      | Parameters |
      | TC_VA_016  |

  @CS-SAPI17
  Scenario Outline: Trigger GET request for TransactionSummaryGetEntities
    Given I trigger GET request for TransactionSummaryGetEntities with parameters "<Parameters>"
    Then I verify the response status is "Success"

    Examples: 
      | Parameters |
      | TC_VA_017  |

  @CS-SAPI18
  Scenario Outline: Trigger GET request for UnConfirmedTxnGetEntities
    Given I trigger GET request for UnConfirmedTxnGetEntities with parameters "<Parameters>"
    Then I verify the response status is "Success"

    Examples: 
      | Parameters |
      | TC_VA_018  |
