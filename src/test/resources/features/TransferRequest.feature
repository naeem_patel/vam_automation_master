@TransferRequest
Feature: Transfer Request Feature

@GTBIPSH-CS612
  Scenario Outline: Create and Approve One Sided Credit Transfer
    Given I am on Login Page
    And I switch to "Alice" User and "HKVAM" entity
    And I am on Transfer Request Page
    When I create Transfer Request for "<Transfer Request ID>"
    Then I verify Transfer Request is in "Submitted" status
    And I switch to "Bob" User and "HKVAM" entity
    And I approve the Transfer Request
    Then I verify Transfer Request is in "Approved" status

    Examples: 
      | Transfer Request ID |
      | TC_VA_011|