package org.igtb.cs.steps;
import java.util.Map;

import org.igtb.itaf.ipsh.cucumber.TestContext;
import org.igtb.itaf.ipsh.steps.AutomationHooks;
import org.igtb.itaf.ipsh.steps.BaseFunctions;
import org.igtb.itaf.ipsh.utilities.ExcelUtils;
import org.igtb.itaf.ipsh.utilities.KeyGenerator;
import org.igtb.itaf.ipsh.utilities.Log;
import org.junit.Assert;
import org.openqa.selenium.By;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class TransferRequestSteps extends BaseFunctions{
	
	TestContext testContext;
	private ExcelUtils excel = new ExcelUtils();
	private Map<String, Map<String, String>> vamExcelData = null;
	private String sTransferRequest = null;
	private Map<String, String> temp = null;

	public TransferRequestSteps (TestContext context) throws Exception {
		super(context);
		testContext = context;
		excel.setExcelFile(AutomationHooks.RESOURCES_PATH + "/TestData/HKVAM.xls", "TransferRequest");
		vamExcelData = excel.getExcelDataIntoHashMap();
	}
	private void waitForRefresh() {
		boolean bFlag = true;
		do {
			try {
				if (driver.getDriver().findElement(By.xpath("//div[@class='datagrid-spinner']")).isDisplayed()) {
					Log.info("Waiting for refresh....");
					Thread.sleep(1000);
					continue;
				} else {
					bFlag = false;
				}
			} catch (Exception e) {
				bFlag = false;
			}
		} while (bFlag == true);
	}
	
	
	@Given("^I am on Transfer Request Page$")
	public void i_am_on_Transfer_Request_Page() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		clickElement("Base VA");
		clickElement("Transaction Management");
		clickElement("Transfer Requests");
		clickElement("REFRESH");
		waitForRefresh();
		
	}
	@When("^I create Transfer Request for \"([^\"]*)\"$")
	public void i_create_Transfer_Request_for(String sTransferRequest) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		this.sTransferRequest = sTransferRequest;
		Map<String, String> vaContent = vamExcelData.get(sTransferRequest);
		clickElement("Add Transfer Request");
		javaScriptClickElement("OneSided");
		javaScriptClickElement("CreditType");
		enterText("Search Physical Account", vaContent.get("PHYACCT"));
		clickElement("LIST");
		enterText("Virtual Account", vaContent.get("VACCT"));
		clickElement("LIST");
		enterText("Currency",vaContent.get("CCY"));
		clickElement("LIST");
		//enterText("ValueDate",vaContent.get("VALUEDATE"));
		enterText("TransactionAmount","100");
		clickElement("VASUBMIT");
		Thread.sleep(10000);
	}


}
