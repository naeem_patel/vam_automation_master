package org.igtb.cs.steps;

import org.igtb.itaf.ipsh.managers.FileReaderManager;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class TransferFile {
	
	
	public void TransferFiles(String origin,String destination) throws JSchException, SftpException
	{
		final String user = FileReaderManager.getInstance().getConfigReader().getProperty("USER_NAME");
		final String host = FileReaderManager.getInstance().getConfigReader().getProperty("HOST_NAME");
		final String password = FileReaderManager.getInstance().getConfigReader().getProperty("PASSWORD");
		java.util.Properties config = new java.util.Properties(); 
		config.put("StrictHostKeyChecking", "no");
		config.put("PreferredAuthentications", "publickey,keyboard-interactive,password");
		
		JSch jsch = new JSch();
		Session session = jsch.getSession(user, host, 22);
		//session.setPort(22);
		session.setConfig(config);
		session.setPassword(password);
		session.connect();
		ChannelSftp sftpChannel = (ChannelSftp) session.openChannel("sftp");
		sftpChannel.connect();
		sftpChannel.put(origin,destination);
		 sftpChannel.disconnect();
		  session.disconnect();
		  
	}
	
	public void PullLogFile(String origin,String destination) throws JSchException, SftpException
	{
		final String user = FileReaderManager.getInstance().getConfigReader().getProperty("USER_NAME");
		final String host = FileReaderManager.getInstance().getConfigReader().getProperty("HOST_NAME");
		final String password = FileReaderManager.getInstance().getConfigReader().getProperty("PASSWORD");
		java.util.Properties config = new java.util.Properties(); 
		config.put("StrictHostKeyChecking", "no");
		config.put("PreferredAuthentications", "publickey,keyboard-interactive,password");
		
		JSch jsch = new JSch();
		Session session = jsch.getSession(user, host, 22);
		//session.setPort(22);
		session.setConfig(config);
		session.setPassword(password);
		session.connect();
		ChannelSftp sftpChannel = (ChannelSftp) session.openChannel("sftp");
		sftpChannel.connect();
		
		sftpChannel.get(origin, destination);
		 sftpChannel.disconnect();
		  session.disconnect();
		  
	}

}


