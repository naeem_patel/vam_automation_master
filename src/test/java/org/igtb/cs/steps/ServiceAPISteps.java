package org.igtb.cs.steps;

import java.util.Map;

import org.igtb.cs.serviceapi.GetAPIRequest;
import org.igtb.itaf.ipsh.cucumber.TestContext;
import org.igtb.itaf.ipsh.managers.FileReaderManager;
import org.igtb.itaf.ipsh.steps.AutomationHooks;
import org.igtb.itaf.ipsh.steps.BaseFunctions;
import org.igtb.itaf.ipsh.utilities.ExcelUtils;
import org.testng.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class ServiceAPISteps  extends BaseFunctions {

	TestContext testContext;
	private ExcelUtils excel = new ExcelUtils();
	private Map<String, Map<String, String>> vamExcelData = null;
	private String sVirtualAccountData = null;
	private Map<String, String> temp = null;
	private String status =null;
	
	public ServiceAPISteps(TestContext context) throws Exception {
		
		super(context);
		testContext = context;
		excel.setExcelFile(AutomationHooks.RESOURCES_PATH + "/TestData/HKVAM.xls", "ServiceAPI");
		vamExcelData = excel.getExcelDataIntoHashMap();
	}
	
	
	@Given("^I trigger GET request for AccountClosureGetEntities with parameters \"([^\"]*)\"$")
	public void i_trigger_GET_request_for_AccountClosureGetEntities_with_parameters(String sVirtualAccountData) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		this.sVirtualAccountData = sVirtualAccountData;
		Map<String, String> vaContent = vamExcelData.get(sVirtualAccountData);
		
		
		String url = FileReaderManager.getInstance().getConfigReader().getProperty("INTERNAL_INTERFACE");
		String armorTicket = FileReaderManager.getInstance().getConfigReader().getProperty("ARMOR_TICKET");
		url = url+vaContent.get("API");
		String bankEntityid = vaContent.get("bankEntityId");
		String param = vaContent.get("param");
		
		
		
	    GetAPIRequest gr = new GetAPIRequest();
	    status = gr.GetAPIRequests(bankEntityid, url, armorTicket, param);
	    
	    
	}
	
	@Given("^I trigger GET request for AccountTransferGetEntities with parameters \"([^\"]*)\"$")
	public void i_trigger_GET_request_for_AccountTransferGetEntities_with_parameters(String sVirtualAccountData) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		this.sVirtualAccountData = sVirtualAccountData;
		Map<String, String> vaContent = vamExcelData.get(sVirtualAccountData);
		
		
		String url = FileReaderManager.getInstance().getConfigReader().getProperty("INTERNAL_INTERFACE");
		String armorTicket = FileReaderManager.getInstance().getConfigReader().getProperty("ARMOR_TICKET");
		url = url+vaContent.get("API");
		String bankEntityid = vaContent.get("bankEntityId");
		String param = vaContent.get("param");
		
		
		
	    GetAPIRequest gr = new GetAPIRequest();
	    status = gr.GetAPIRequests(bankEntityid, url, armorTicket, param);  
	}
	
	@Given("^I trigger GET request for AllocationSummaryGetEntities with parameters \"([^\"]*)\"$")
	public void i_trigger_GET_request_for_AllocationSummaryGetEntities_with_parameters(String sVirtualAccountData) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		this.sVirtualAccountData = sVirtualAccountData;
		Map<String, String> vaContent = vamExcelData.get(sVirtualAccountData);
		
		
		String url = FileReaderManager.getInstance().getConfigReader().getProperty("INTERNAL_INTERFACE");
		String armorTicket = FileReaderManager.getInstance().getConfigReader().getProperty("ARMOR_TICKET");
		url = url+vaContent.get("API");
		String bankEntityid = vaContent.get("bankEntityId");
		String param = vaContent.get("param");
		
		
		
	    GetAPIRequest gr = new GetAPIRequest();
	    status = gr.GetAPIRequests(bankEntityid, url, armorTicket, param);  
	}
	
	@Given("^I trigger GET request for BankConfigurationGetEntities with parameters \"([^\"]*)\"$")
	public void i_trigger_GET_request_for_BankConfigurationGetEntities_with_parameters(String sVirtualAccountData) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		this.sVirtualAccountData = sVirtualAccountData;
		Map<String, String> vaContent = vamExcelData.get(sVirtualAccountData);
		
		
		String url = FileReaderManager.getInstance().getConfigReader().getProperty("INTERNAL_INTERFACE");
		String armorTicket = FileReaderManager.getInstance().getConfigReader().getProperty("ARMOR_TICKET");
		url = url+vaContent.get("API");
		String bankEntityid = vaContent.get("bankEntityId");
		String param = vaContent.get("param");
		
		
		
	    GetAPIRequest gr = new GetAPIRequest();
	    status = gr.GetAPIRequests(bankEntityid, url, armorTicket, param);  
	}
	
	@Given("^I trigger GET request for CustomerInterestBVLGetEntities with parameters \"([^\"]*)\"$")
	public void i_trigger_GET_request_for_CustomerInterestBVLGetEntities_with_parameters(String sVirtualAccountData) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		this.sVirtualAccountData = sVirtualAccountData;
		Map<String, String> vaContent = vamExcelData.get(sVirtualAccountData);
		
		
		String url = FileReaderManager.getInstance().getConfigReader().getProperty("INTERNAL_INTERFACE");
		String armorTicket = FileReaderManager.getInstance().getConfigReader().getProperty("ARMOR_TICKET");
		url = url+vaContent.get("API");
		String bankEntityid = vaContent.get("bankEntityId");
		String param = vaContent.get("param");
		
		
		
	    GetAPIRequest gr = new GetAPIRequest();
	    status = gr.GetAPIRequests(bankEntityid, url, armorTicket, param);  
	}
	
	
	@Given("^I trigger GET request for BVLProfileLinkageGetEntities with parameters \"([^\"]*)\"$")
	public void i_trigger_GET_request_for_BVLProfileLinkageGetEntities_with_parameters(String sVirtualAccountData) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		this.sVirtualAccountData = sVirtualAccountData;
		Map<String, String> vaContent = vamExcelData.get(sVirtualAccountData);
		
		
		String url = FileReaderManager.getInstance().getConfigReader().getProperty("INTERNAL_INTERFACE");
		String armorTicket = FileReaderManager.getInstance().getConfigReader().getProperty("ARMOR_TICKET");
		url = url+vaContent.get("API");
		String bankEntityid = vaContent.get("bankEntityId");
		String param = vaContent.get("param");
		
		
		
	    GetAPIRequest gr = new GetAPIRequest();
	    status = gr.GetAPIRequests(bankEntityid, url, armorTicket, param);  
	}
	
	@Given("^I trigger GET request for ContextIDIssuGetEntities with parameters \"([^\"]*)\"$")
	public void i_trigger_GET_request_for_ContextIDIssuGetEntities_with_parameters(String sVirtualAccountData) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		this.sVirtualAccountData = sVirtualAccountData;
		Map<String, String> vaContent = vamExcelData.get(sVirtualAccountData);
		
		
		String url = FileReaderManager.getInstance().getConfigReader().getProperty("INTERNAL_INTERFACE");
		String armorTicket = FileReaderManager.getInstance().getConfigReader().getProperty("ARMOR_TICKET");
		url = url+vaContent.get("API");
		String bankEntityid = vaContent.get("bankEntityId");
		String param = vaContent.get("param");
		
		
		
	    GetAPIRequest gr = new GetAPIRequest();
	    status = gr.GetAPIRequests(bankEntityid, url, armorTicket, param);  
	}
	
	@Given("^I trigger GET request for ContextIDRequestGetEntities with parameters \"([^\"]*)\"$")
	public void i_trigger_GET_request_for_ContextIDRequestGetEntities_with_parameters(String sVirtualAccountData) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		this.sVirtualAccountData = sVirtualAccountData;
		Map<String, String> vaContent = vamExcelData.get(sVirtualAccountData);
		
		
		String url = FileReaderManager.getInstance().getConfigReader().getProperty("INTERNAL_INTERFACE");
		String armorTicket = FileReaderManager.getInstance().getConfigReader().getProperty("ARMOR_TICKET");
		url = url+vaContent.get("API");
		String bankEntityid = vaContent.get("bankEntityId");
		String param = vaContent.get("param");
		
		
		
	    GetAPIRequest gr = new GetAPIRequest();
	    status = gr.GetAPIRequests(bankEntityid, url, armorTicket, param);  
	}
	
	
	
	@Given("^I trigger GET request for CustomerIntProfileGetEntities with parameters \"([^\"]*)\"$")
	public void i_trigger_GET_request_for_CustomerIntProfileGetEntities_with_parameters(String sVirtualAccountData) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		this.sVirtualAccountData = sVirtualAccountData;
		Map<String, String> vaContent = vamExcelData.get(sVirtualAccountData);
		
		
		String url = FileReaderManager.getInstance().getConfigReader().getProperty("INTERNAL_INTERFACE");
		String armorTicket = FileReaderManager.getInstance().getConfigReader().getProperty("ARMOR_TICKET");
		url = url+vaContent.get("API");
		String bankEntityid = vaContent.get("bankEntityId");
		String param = vaContent.get("param");

		
	    GetAPIRequest gr = new GetAPIRequest();
	    status = gr.GetAPIRequests(bankEntityid, url, armorTicket, param);  
	}


	
	
	@Given("^I trigger GET request for DashboardGetIssuanceMetric with parameters \"([^\"]*)\"$")
	public void i_trigger_GET_request_for_DashboardGetIssuanceMetric_with_parameters(String sVirtualAccountData) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		this.sVirtualAccountData = sVirtualAccountData;
		Map<String, String> vaContent = vamExcelData.get(sVirtualAccountData);
		
		
		String url = FileReaderManager.getInstance().getConfigReader().getProperty("INTERNAL_INTERFACE");
		String armorTicket = FileReaderManager.getInstance().getConfigReader().getProperty("ARMOR_TICKET");
		url = url+vaContent.get("API");
		String bankEntityid = vaContent.get("bankEntityId");
		String param = vaContent.get("param");

		
	    GetAPIRequest gr = new GetAPIRequest();
	    status = gr.GetAPIRequests(bankEntityid, url, armorTicket, param);  
	}
	
	
	
	
	@Given("^I trigger GET request for DashboardGetPhysicalAcct with parameters \"([^\"]*)\"$")
	public void i_trigger_GET_request_for_DashboardGetPhysicalAcct_with_parameters(String sVirtualAccountData) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		this.sVirtualAccountData = sVirtualAccountData;
		Map<String, String> vaContent = vamExcelData.get(sVirtualAccountData);
		
		
		String url = FileReaderManager.getInstance().getConfigReader().getProperty("INTERNAL_INTERFACE");
		String armorTicket = FileReaderManager.getInstance().getConfigReader().getProperty("ARMOR_TICKET");
		url = url+vaContent.get("API");
		String bankEntityid = vaContent.get("bankEntityId");
		String param = vaContent.get("param");

		
	    GetAPIRequest gr = new GetAPIRequest();
	    status = gr.GetAPIRequests(bankEntityid, url, armorTicket, param);  
	}
	
	
	
	@Given("^I trigger GET request for DashboardGetVirtualAcct with parameters \"([^\"]*)\"$")
	public void i_trigger_GET_request_for_DashboardGetVirtualAcct_with_parameters(String sVirtualAccountData) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		this.sVirtualAccountData = sVirtualAccountData;
		Map<String, String> vaContent = vamExcelData.get(sVirtualAccountData);
		
		
		String url = FileReaderManager.getInstance().getConfigReader().getProperty("INTERNAL_INTERFACE");
		String armorTicket = FileReaderManager.getInstance().getConfigReader().getProperty("ARMOR_TICKET");
		url = url+vaContent.get("API");
		String bankEntityid = vaContent.get("bankEntityId");
		String param = vaContent.get("param");

		
	    GetAPIRequest gr = new GetAPIRequest();
	    status = gr.GetAPIRequests(bankEntityid, url, armorTicket, param);  
	}
	
	
	@Given("^I trigger GET request for ProductConfigGetEntities with parameters \"([^\"]*)\"$")
	public void i_trigger_GET_request_for_ProductConfigGetEntities_with_parameters(String sVirtualAccountData) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		this.sVirtualAccountData = sVirtualAccountData;
		Map<String, String> vaContent = vamExcelData.get(sVirtualAccountData);
		
		
		String url = FileReaderManager.getInstance().getConfigReader().getProperty("INTERNAL_INTERFACE");
		String armorTicket = FileReaderManager.getInstance().getConfigReader().getProperty("ARMOR_TICKET");
		url = url+vaContent.get("API");
		String bankEntityid = vaContent.get("bankEntityId");
		String param = vaContent.get("param");

		
	    GetAPIRequest gr = new GetAPIRequest();
	    status = gr.GetAPIRequests(bankEntityid, url, armorTicket, param);  
	}
	
	
	@Given("^I trigger GET request for SubAcctIssuanceGetEntities with parameters \"([^\"]*)\"$")
	public void i_trigger_GET_request_for_SubAcctIssuanceGetEntities_with_parameters(String sVirtualAccountData) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		this.sVirtualAccountData = sVirtualAccountData;
		Map<String, String> vaContent = vamExcelData.get(sVirtualAccountData);
		
		
		String url = FileReaderManager.getInstance().getConfigReader().getProperty("INTERNAL_INTERFACE");
		String armorTicket = FileReaderManager.getInstance().getConfigReader().getProperty("ARMOR_TICKET");
		url = url+vaContent.get("API");
		String bankEntityid = vaContent.get("bankEntityId");
		String param = vaContent.get("param");

		
	    GetAPIRequest gr = new GetAPIRequest();
	    status = gr.GetAPIRequests(bankEntityid, url, armorTicket, param);  
	}
	
	
	
	@Given("^I trigger GET request for SubAcctIssuanceReqGetEntities with parameters \"([^\"]*)\"$")
	public void i_trigger_GET_request_for_SubAcctIssuanceReqGetEntities_with_parameters(String sVirtualAccountData) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		this.sVirtualAccountData = sVirtualAccountData;
		Map<String, String> vaContent = vamExcelData.get(sVirtualAccountData);
		
		
		String url = FileReaderManager.getInstance().getConfigReader().getProperty("INTERNAL_INTERFACE");
		String armorTicket = FileReaderManager.getInstance().getConfigReader().getProperty("ARMOR_TICKET");
		url = url+vaContent.get("API");
		String bankEntityid = vaContent.get("bankEntityId");
		String param = vaContent.get("param");

		
	    GetAPIRequest gr = new GetAPIRequest();
	    status = gr.GetAPIRequests(bankEntityid, url, armorTicket, param);  
	}
	
	@Given("^I trigger GET request for SubAcctPrefixConfigGetEntities with parameters \"([^\"]*)\"$")
	public void i_trigger_GET_request_for_SubAcctPrefixConfigGetEntities_with_parameters(String sVirtualAccountData) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		this.sVirtualAccountData = sVirtualAccountData;
		Map<String, String> vaContent = vamExcelData.get(sVirtualAccountData);
		
		
		String url = FileReaderManager.getInstance().getConfigReader().getProperty("INTERNAL_INTERFACE");
		String armorTicket = FileReaderManager.getInstance().getConfigReader().getProperty("ARMOR_TICKET");
		url = url+vaContent.get("API");
		String bankEntityid = vaContent.get("bankEntityId");
		String param = vaContent.get("param");

		
	    GetAPIRequest gr = new GetAPIRequest();
	    status = gr.GetAPIRequests(bankEntityid, url, armorTicket, param);  
	}
	
	
	
	@Given("^I trigger GET request for TransactionSummaryGetEntities with parameters \"([^\"]*)\"$")
	public void i_trigger_GET_request_for_TransactionSummaryGetEntities_with_parameters(String sVirtualAccountData) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		this.sVirtualAccountData = sVirtualAccountData;
		Map<String, String> vaContent = vamExcelData.get(sVirtualAccountData);
		
		
		String url = FileReaderManager.getInstance().getConfigReader().getProperty("INTERNAL_INTERFACE");
		String armorTicket = FileReaderManager.getInstance().getConfigReader().getProperty("ARMOR_TICKET");
		url = url+vaContent.get("API");
		String bankEntityid = vaContent.get("bankEntityId");
		String param = vaContent.get("param");

		
	    GetAPIRequest gr = new GetAPIRequest();
	    status = gr.GetAPIRequests(bankEntityid, url, armorTicket, param);  
	}
	
	@Given("^I trigger GET request for UnConfirmedTxnGetEntities with parameters \"([^\"]*)\"$")
	public void i_trigger_GET_request_for_UnConfirmedTxnGetEntities_with_parameters(String sVirtualAccountData) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		this.sVirtualAccountData = sVirtualAccountData;
		Map<String, String> vaContent = vamExcelData.get(sVirtualAccountData);
		
		
		String url = FileReaderManager.getInstance().getConfigReader().getProperty("INTERNAL_INTERFACE");
		String armorTicket = FileReaderManager.getInstance().getConfigReader().getProperty("ARMOR_TICKET");
		url = url+vaContent.get("API");
		String bankEntityid = vaContent.get("bankEntityId");
		String param = vaContent.get("param");

		
	    GetAPIRequest gr = new GetAPIRequest();
	    status = gr.GetAPIRequests(bankEntityid, url, armorTicket, param);  
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Then("^I verify the response status is \"([^\"]*)\"$")
	public void i_verify_the_response_status_is(String vStatus) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		Assert.assertEquals(status, vStatus);
	}
}
