package org.igtb.cs.steps;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import org.igtb.itaf.ipsh.cucumber.TestContext;
import org.igtb.itaf.ipsh.steps.BaseFunctions;
import org.igtb.itaf.ipsh.utilities.KeyGenerator;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Assert;

public class FieldValidations extends BaseFunctions {
	TestContext testContext;

	private String screename;

	public FieldValidations(TestContext context) {

		super(context);
		// TODO Auto-generated constructor stub
		testContext = context;
	}
	public void validation(String args[], String valid, int num, String webElement, String screename) throws Exception {
		int i = args.length;

		for (int j = 0; j < i; j++) {
			String msg = null;
			switch (args[j]) {

			case "VCHARLW":
				if (args[j].equals(valid)) {
					enterText(webElement, KeyGenerator.getRandomString(num + 1).toLowerCase());
					clickElement("CLICKVA");
					msg = getText("FieldErrorMsg", getErrorMessage(screename, webElement, args[j]));
					clickElement("CLICKVA");
					msg = msg.trim();
					Assert.assertEquals(msg, getErrorMessage(screename, webElement, args[j]));

				} else {
					enterText(webElement, KeyGenerator.getRandomString(num).toLowerCase());
					clickElement("CLICKVA");
					msg = getText("FieldErrorMsg", getErrorMessage(screename, webElement, args[j]));
					clickElement("CLICKVA");
					msg = msg.trim();
					Assert.assertEquals(msg, getErrorMessage(screename, webElement, args[j]));

				}

				break;
			case "VCHARUP":
				if (args[j].equals(valid)) {
					enterText(webElement, KeyGenerator.getRandomString(num + 1).toUpperCase());
					clickElement("CLICKVA");
					msg = getText("FieldErrorMsg", getErrorMessage(screename, webElement, args[j]));
					clickElement("CLICKVA");
					msg = msg.trim();
					Assert.assertEquals(msg, getErrorMessage(screename, webElement, args[j]));
				} else {
					enterText(webElement, KeyGenerator.getRandomString(num).toUpperCase());
					clickElement("CLICKVA");
					msg = getText("FieldErrorMsg", getErrorMessage(screename, webElement, args[j]));
					clickElement("CLICKVA");
					msg = msg.trim();
					Assert.assertEquals(msg, getErrorMessage(screename, webElement, args[j]));
				}
				break;
			case "VALFHA":
				if (args[j].equals(valid)) {
					enterText(webElement, KeyGenerator.getrandomAlphanumericString(num + 1));
					clickElement("CLICKVA");
					msg = getText("FieldErrorMsg", getErrorMessage(screename, webElement, args[j]));
					msg = msg.trim();
					Assert.assertEquals(msg, getErrorMessage(screename, webElement, args[j]));
				} else {
					enterText(webElement, KeyGenerator.getrandomAlphanumericString(num));
					clickElement("CLICKVA");
					msg = getText("FieldErrorMsg", getErrorMessage(screename, webElement, args[j]));
					msg = msg.trim();
					Assert.assertEquals(msg, getErrorMessage(screename, webElement, args[j]));
				}
				break;
			case "VNUM":
				if (args[j].equals(valid)) {
					enterText(webElement, KeyGenerator.getRandomInteger(num + 1));
					clickElement("CLICKVA");
					msg = getText("FieldErrorMsg", getErrorMessage(screename, webElement, args[j]));
					msg = msg.trim();
					Assert.assertEquals(msg, getErrorMessage(screename, webElement, args[j]));
				} else {
					enterText(webElement, KeyGenerator.getRandomInteger(num));
					clickElement("CLICKVA");
					msg = getText("FieldErrorMsg", getErrorMessage(screename, webElement, args[j]));
					msg = msg.trim();
					Assert.assertEquals(msg, getErrorMessage(screename, webElement, args[j]));
				}
				break;
			case "BLANK":

				enterText(webElement, "");
				clickElement("CLICKVA"); // Only for VAM
				msg = getText("FieldErrorMsg", getErrorMessage(screename, webElement, args[j]));
				msg = msg.trim();
				Assert.assertEquals(msg, getErrorMessage(screename, webElement, args[j]));
				break;
			case "EMAIL":
				ArrayList<String> vemail = new ArrayList<String>();
				vemail.add("@email@domain.com");
				vemail.add("#email@domain.com");
				vemail.add("@domain.com");
				vemail.add("@domain.co.in");
				vemail.add("email@domincom");
				vemail.add("emaildomain.com");
				vemail.add("email#domain.com");

				for (int k = 0; k < vemail.size(); k++) {
					enterText(webElement, vemail.get(k));
					clickElement("CLICKVA");
					msg = getText("FieldErrorMsg", getErrorMessage(screename, webElement, args[j]));
					msg = msg.trim();
					Assert.assertEquals(msg, getErrorMessage(screename, webElement, args[j]));

				}
				break;

			}
		}
	}

	public void gridValidation(String args[], String valid, int num, String webElement, String screename)
			throws Exception {
		int i = args.length;

		for (int j = 0; j < i; j++) {
			String msg = null;
			switch (args[j]) {

			case "VCHARLW":
				if (args[j].equals(valid)) {
					enterText(webElement, KeyGenerator.getRandomString(num + 1).toLowerCase());
					clickElement("CLICKVA");
					msg = getText("GridErrorMsg", getErrorMessage(screename, webElement, args[j]));
					clickElement("CLICKVA");
					msg = msg.trim();
					Assert.assertEquals(msg, getErrorMessage(screename, webElement, args[j]));

				} else {
					enterText(webElement, KeyGenerator.getRandomString(num).toLowerCase());
					clickElement("CLICKVA");
					msg = getText("GridErrorMsg", getErrorMessage(screename, webElement, args[j]));
					clickElement("CLICKVA");
					msg = msg.trim();
					Assert.assertEquals(msg, getErrorMessage(screename, webElement, args[j]));

				}

				break;
			case "VCHARUP":
				if (args[j].equals(valid)) {
					enterText(webElement, KeyGenerator.getRandomString(num + 1).toUpperCase());
					clickElement("CLICKVA");
					msg = getText("GridErrorMsg", getErrorMessage(screename, webElement, args[j]));
					clickElement("CLICKVA");
					msg = msg.trim();
					Assert.assertEquals(msg, getErrorMessage(screename, webElement, args[j]));
				} else {
					enterText(webElement, KeyGenerator.getRandomString(num).toUpperCase());
					clickElement("CLICKVA");
					msg = getText("GridErrorMsg", getErrorMessage(screename, webElement, args[j]));
					clickElement("CLICKVA");
					msg = msg.trim();
					Assert.assertEquals(msg, getErrorMessage(screename, webElement, args[j]));
				}
				break;
			case "VALFHA":
				if (args[j].equals(valid)) {
					enterText(webElement, KeyGenerator.getrandomAlphanumericString(num + 1));
					clickElement("CLICKVA");
					msg = getText("GridErrorMsg", getErrorMessage(screename, webElement, args[j]));
					msg = msg.trim();
					Assert.assertEquals(msg, getErrorMessage(screename, webElement, args[j]));
				} else {
					enterText(webElement, KeyGenerator.getrandomAlphanumericString(num));
					clickElement("CLICKVA");
					msg = getText("GridErrorMsg", getErrorMessage(screename, webElement, args[j]));
					msg = msg.trim();
					Assert.assertEquals(msg, getErrorMessage(screename, webElement, args[j]));
				}
				break;
			case "VNUM":
				if (args[j].equals(valid)) {
					enterText(webElement, KeyGenerator.getRandomInteger(num + 1));
					clickElement("CLICKVA");
					msg = getText("GridErrorMsg", getErrorMessage(screename, webElement, args[j]));
					msg = msg.trim();
					Assert.assertEquals(msg, getErrorMessage(screename, webElement, args[j]));
				} else {
					enterText(webElement, KeyGenerator.getRandomInteger(num));
					clickElement("CLICKVA");
					msg = getText("GridErrorMsg", getErrorMessage(screename, webElement, args[j]));
					msg = msg.trim();
					Assert.assertEquals(msg, getErrorMessage(screename, webElement, args[j]));
				}
				break;
			case "BLANK":

				enterText(webElement, "");
				clickElement("CLICKVA"); // Only for VAM
				msg = getText("GridErrorMsg", getErrorMessage(screename, webElement, args[j]));
				msg = msg.trim();
				Assert.assertEquals(msg, getErrorMessage(screename, webElement, args[j]));
				break;
			case "EMAIL":
				ArrayList<String> vemail = new ArrayList<String>();
				vemail.add("@email@domain.com");
				vemail.add("#email@domain.com");
				vemail.add("@domain.com");
				vemail.add("@domain.co.in");
				vemail.add("email@domincom");
				vemail.add("emaildomain.com");
				vemail.add("email#domain.com");

				for (int k = 0; k < vemail.size(); k++) {
					enterText(webElement, vemail.get(k));
					clickElement("CLICKVA");
					msg = getText("GridErrorMsg", getErrorMessage(screename, webElement, args[j]));
					msg = msg.trim();
					Assert.assertEquals(msg, getErrorMessage(screename, webElement, args[j]));

				}
				break;

			}
		}
	}

	public String getErrorMessage(String screename, String webElement, String val) {
		String errorMsg = null;
		JSONParser parser = new JSONParser();
		try {

			Object obj = parser.parse(new FileReader(
					"D:\\CommonService_TestAutomation\\vam_automation_master\\src\\test\\resources\\TestData\\VAErrorMsg.json")); // property
																																	// file
																																	// driven
			JSONObject jsonObjectParent = (JSONObject) obj;
			Map<String, String> parentNode = (Map<String, String>) jsonObjectParent.get(screename);
			JSONObject jsonObjectChild = (JSONObject) parentNode;
			Map<String, String> childNode = (Map<String, String>) jsonObjectChild.get(webElement);
			errorMsg = childNode.get(val);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return errorMsg;
	}

}
