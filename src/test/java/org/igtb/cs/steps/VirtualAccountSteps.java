package org.igtb.cs.steps;

import java.util.Map;

import org.igtb.itaf.ipsh.cucumber.TestContext;
import org.igtb.itaf.ipsh.managers.FileReaderManager;
import org.igtb.itaf.ipsh.steps.AutomationHooks;
import org.igtb.itaf.ipsh.steps.BaseFunctions;
import org.igtb.itaf.ipsh.utilities.ExcelUtils;
import org.igtb.itaf.ipsh.utilities.KeyGenerator;
import org.igtb.itaf.ipsh.utilities.Log;
import org.junit.Assert;
import org.openqa.selenium.By;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class VirtualAccountSteps extends BaseFunctions {
	TestContext testContext;
	private ExcelUtils excel = new ExcelUtils();
	private Map<String, Map<String, String>> vamExcelData = null;
	private String sVirtualAccountData = null;
	private Map<String, String> temp = null;

	public VirtualAccountSteps(TestContext context) throws Exception {
		super(context);
		testContext = context;
		excel.setExcelFile(AutomationHooks.RESOURCES_PATH + "/TestData/HKVAM.xls", "VirtualAccount");
		vamExcelData = excel.getExcelDataIntoHashMap();
	}

	private void waitForRefresh() {
		boolean bFlag = true;
		do {
			try {
				if (driver.getDriver().findElement(By.xpath("//div[@class='datagrid-spinner']")).isDisplayed()) {
					Log.info("Waiting for refresh....");
					Thread.sleep(1000);
					continue;
				} else {
					bFlag = false;
				}
			} catch (Exception e) {
				bFlag = false;
			}
		} while (bFlag == true);
	}

	private void waitForListVisible() {
		boolean bFlag = true;
		do {
			try {
				if (driver.getDriver().findElement(By.xpath("//div[@class='list']")).isDisplayed()) {
					Log.info("Waiting for List to Visible....");
					Thread.sleep(1000);
					continue;
				} else {
					bFlag = false;
				}
			} catch (Exception e) {
				bFlag = false;
			}
		} while (bFlag == true);
	}

	String customer;
	String phyAcct;
	String vaproduct;
	String subprefix;
	String VASTRUCT;
	String VASTRUCTLEN;
	String VASUBACCTLEN;
	String ISCONTEXT;
	String CONTEXTTYPE;
	String CONTEXTLENGTH;
	
	@Given("^I switch to \"([^\"]*)\" User and \"([^\"]*)\" entity$")
	public void i_switch_to_User_and_entity(String userid, String bankentityId) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		clickElement("User");
		enterText("Custom name", userid);
		enterText("Custom entity ID", bankentityId);
		clickElement("Set custom user");
		Thread.sleep(1000);
		clickElement("CLICKVA");

	}

	@Given("^I am on Sub Account Prefix Page$")
	public void i_am_on_Sub_Account_Prefix_Page() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		clickElement("Base VA");
		clickElement("Account Management");
		clickElement("VA Structure Configuration");
		clickElement("REFRESH");
		waitForRefresh();
	}

	@Given("^I am on Sub Account Issuance Request Page$")
	public void i_am_on_Sub_Account_Issuance_Request_Page() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		clickElement("Base VA");
		clickElement("Account Management");
		clickElement("Sub Account Issuance Request");
		clickElement("REFRESH");
		waitForRefresh();
	}

	@Given("^I am on Sub Account Maintenance Page$")
	public void i_am_on_Sub_Account_Maintenance_Page() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		clickElement("Base VA");
		clickElement("Account Management");
		clickElement("Sub Account Maintenance");
		clickElement("REFRESH");
		waitForRefresh();
	}
	
	@Given("^I navigate to Sub Account Maintenance Page$")
	public void i_navigate_to_Sub_Account_Maintenance_Page() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		//clickElement("Base VA");
		//clickElement("Account Management");
		clickElement("Sub Account Maintenance");
		clickElement("REFRESH");
		waitForRefresh();
	}
	
	@Given("^I am on Context ID Issuance Page$")
	public void i_am_on_Context_ID_Issuance_Page() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		clickElement("Base VA");
		clickElement("Account Management");
		clickElement("Context Id Issuance Request");
		clickElement("REFRESH");
		waitForRefresh();
	}

	@Given("^I am on Context ID Maintenance Page$")
	public void i_am_on_Context_ID_Maintenance_Page() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		clickElement("Base VA");
		clickElement("Account Management");
		clickElement("Context ID Maintenance");
		clickElement("REFRESH");
		waitForRefresh();
	}

	@When("^I add Sub Account Prefix for \"([^\"]*)\"$")
	public void i_add_Sub_Account_Prefix_for(String sVirtualAccountData) throws Exception {
		// Write code here that turns the phrase above into concrete actions
		this.sVirtualAccountData = sVirtualAccountData;
		temp = CSAutomationHooks.virtualaccountJSONData.get(this.sVirtualAccountData);
		Map<String, String> vaContent = vamExcelData.get(sVirtualAccountData);
		// temp.put("Product", vaContent.get("Product"));
		clickElement("Add VA Structure");
		enterText("Customer", vaContent.get("CUSTOMER"));
		waitForListVisible();
		clickElement("LIST");
		customer = vaContent.get("CUSTOMER");
		enterText("Physical Account", vaContent.get("PHYACCT"));
		waitForListVisible();
		clickElement("LIST");
		phyAcct = vaContent.get("PHYACCT");
		enterText("VAProduct", vaContent.get("PRODUCT"));
		waitForListVisible();
		clickElement("LIST");
		vaproduct = vaContent.get("PRODUCT");

		if (vaContent.get("VASTRUCT").equals("Manual")) {
			enterText("Enter VA Structure",
					KeyGenerator.getRandomInteger(Integer.parseInt(vaContent.get("VASTRUCTLEN"))));

		}

		if (vaContent.get("ISCONTEXT").equals("Yes")) {
			javaScriptClickElement("ContextIDY");
			Thread.sleep(10000);
			if (vaContent.get("CONTEXTTYPE").equals("Manual")) {
				javaScriptClickElement("ContextIDIssuanceType");
			}
			enterText("ContextID Length", vaContent.get("CONTEXTLENGTH"));
		}
	//	javaScriptClickElement("Interest Defination");
		Thread.sleep(10000);
		clickElement("VASUBMIT");
		Thread.sleep(5000);
		temp.put("Customer", vaContent.get("CUSTOMER"));
		temp.put("PhyAcct", vaContent.get("PHYACCT"));
		temp.put("Product", vaContent.get("PRODUCT"));

	}

	@When("^I add Sub Account Issuance Request for \"([^\"]*)\"$")
	public void i_add_Sub_Account_Issuance_Request_for(String sVirtualAccountData) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		this.sVirtualAccountData = sVirtualAccountData;
		temp = CSAutomationHooks.virtualaccountJSONData.get(this.sVirtualAccountData);
		Map<String, String> vaContent = vamExcelData.get(sVirtualAccountData);
		clickElement("Add VA Issuance Request");
		enterText("Customer", temp.get("Customer"));
		waitForListVisible();
		clickElement("LIST");

		enterText("Physical Account", temp.get("PhyAcct"));
		waitForListVisible();
		clickElement("LIST");

		enterText("VA Structure", temp.get("SubAcctPrefix"));
		waitForListVisible();
		clickElement("LIST");

		if (vaContent.get("VASTRUCT").equals("Manual")) {

			enterText("Enter Sub Account Number",
					KeyGenerator.getRandomInteger(Integer.parseInt(vaContent.get("VASUBACCTLEN"))));
			enterText("Enter Account Owner Name", "AUTO" + KeyGenerator.getDate("CurrentDate", "ddMMyyyyHHmmss"));

		} else {
			enterText("No.of Accounts", "1");
		}

		Thread.sleep(5000);
		clickElement("VASUBMIT");
		Thread.sleep(5000);

	}

	@When("^I add Context ID Issuance Request for \"([^\"]*)\"$")
	public void i_add_Context_ID_Issuance_Request_for(String sVirtualAccountData) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		this.sVirtualAccountData = sVirtualAccountData;
		temp = CSAutomationHooks.virtualaccountJSONData.get(this.sVirtualAccountData);
		Map<String, String> vaContent = vamExcelData.get(sVirtualAccountData);
		clickElement("Add Context ID Request");

		enterText("VA Structure", temp.get("SubAcctPrefix"));
		waitForListVisible();
		clickElement("LIST");

		enterText("ContextVAAccount", temp.get("virtualAcct"));
		waitForListVisible();
		clickElement("LIST");

		if (vaContent.get("CONTEXTTYPE").equals("Manual")) {

			enterText("Enter Context Identifier",
					KeyGenerator.getRandomInteger(Integer.parseInt(vaContent.get("CONTEXTLENGTH"))));

		} else {
			enterText("No.of Accounts", "1");
		}

		Thread.sleep(5000);
		clickElement("VASUBMIT");
		Thread.sleep(5000);

	}

	@Then("^I verify Sub Account Prefix is in \"([^\"]*)\" status$")
	public void i_verify_Sub_Account_Prefix_is_in_status(String sStatus) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		String aStatus;
		Thread.sleep(5000);
		clickElement("VA Structure PhyAcct Filter");
		enterText("Search Filter", phyAcct);
		Thread.sleep(5000);
		clickElement("CLICKVA");
		Thread.sleep(5000);
		clickElement("VA Structure Product Filter");
		enterText("Search Filter", vaproduct);
		Thread.sleep(5000);
		clickElement("CLICKVA");
		aStatus = getText("GetStatus");
		Assert.assertTrue(aStatus.contentEquals(sStatus));
		if(sStatus.equals("Approved"))
		{
			temp.put("SubAcctPrefix", getText("GetPrefix"));
		}

	}

	@Then("^I approve the Sub Account Prefix$")
	public void i_approve_the_record() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(5000);
		javaScriptClickElement("VA Structure CheckBox");
		clickElement("ApproveVA");
		clickElement("APPROVE Remarks");
		Thread.sleep(5000);
		clickElement("ClearFilters");

	}
	
	@Then("^I reject the Sub Account Prefix$")
	public void i_reject_the_record() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(5000);
		javaScriptClickElement("VA Structure CheckBox");
		clickElement("RejectVA");
		enterText("VARemarks","Rejected");
		clickElement("VARejectRemarks");
		Thread.sleep(5000);
		System.out.println(getText("ALERTDIALOG"));
	   Assert.assertEquals(getText("ALERTDIALOG"),"The record(s) are rejected successfully");	

	}
	
	@Then("^I reject the Sub Account Issuance Request$")
	public void i_reject_the_Sub_Account_Issuance_Request() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(5000);
		javaScriptClickElement("VA Structure CheckBox");
		clickElement("RejectVA");
		enterText("VARemarks","Rejected");
		clickElement("VARejectRemarks");
		Thread.sleep(5000);
		System.out.println(getText("ALERTDIALOG"));
	   Assert.assertEquals(getText("ALERTDIALOG"),"The record(s) are rejected successfully");	

	}

	@Then("^I verify Sub Account Issuance Request is in \"([^\"]*)\" status$")
	public void i_verify_Sub_Account_Issuance_Request_is_in_status(String sStatus) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		String aStatus;
		waitForPageLoad();
		waitForRefresh();
		Thread.sleep(15000);
		clickElement("VA Structure PhyAcct Filter");
		enterText("Search Filter", temp.get("PhyAcct"));
		Thread.sleep(5000);
		clickElement("CLICKVA");
		Thread.sleep(5000);
		clickElement("VA Structure Product Filter");
		enterText("Search Filter", temp.get("Product"));
		Thread.sleep(5000);
		clickElement("CLICKVA");
		javaScriptClickElement("VA Structure Sub Account Prefix Filter");
		enterText("Search Filter", temp.get("SubAcctPrefix"));
		Thread.sleep(5000);
		clickElement("CLICKVA");
		aStatus = getText("GetStatus");
		Assert.assertTrue(aStatus.contentEquals(sStatus));

	}

	@Then("^I approve the Sub Account Issuance Request$")
	public void i_approve_the_sub_accout_issuance_request() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(5000);
		javaScriptClickElement("VA Structure CheckBox");
		clickElement("ApproveVA");
		clickElement("APPROVE Remarks");
		Thread.sleep(5000);
		clickElement("ClearFilters");
		//Assert.assertEquals(getText("ALERTDIALOG"),"");
		System.out.println(getText("ALERTDIALOG"));
	}

	@Then("^I verify Sub Account is in \"([^\"]*)\" status for \"([^\"]*)\"$")
	public void i_verify_Sub_Account_is_in_status_for(String sStatus, String sVirtualAccountData) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		String aStatus;
		this.sVirtualAccountData = sVirtualAccountData;
		temp = CSAutomationHooks.virtualaccountJSONData.get(this.sVirtualAccountData);
//		Thread.sleep(5000);
//		clickElement("VA Structure PhyAcct Filter");
//		enterText("Search Filter", temp.get("PhyAcct"));
//		Thread.sleep(5000);
//		clickElement("CLICKVA");
//		Thread.sleep(5000);
//		javaScriptClickElement("VA Structure Sub Account Prefix Filter");
//		enterText("Search Filter", temp.get("SubAcctPrefix"));
//		Thread.sleep(5000);
//		clickElement("CLICKVA");
		aStatus = getText("GetStatus");
		Assert.assertTrue(aStatus.contentEquals(sStatus));
		temp.put("virtualAcct", getText("GetVirtualAcct"));

	}

	@Then("^I verify Context ID is in \"([^\"]*)\" status for \"([^\"]*)\"$")
	public void i_verify_context_id_is_in_status_for(String sStatus, String sVirtualAccountData) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		String aStatus;
		this.sVirtualAccountData = sVirtualAccountData;
		temp = CSAutomationHooks.virtualaccountJSONData.get(this.sVirtualAccountData);
		Thread.sleep(5000);
		clickElement("VA Structure PhyAcct Filter");
		enterText("Search Filter", temp.get("PhyAcct"));
		Thread.sleep(5000);
		clickElement("CLICKVA");
		Thread.sleep(5000);
		javaScriptClickElement("VA Structure Sub Account Prefix Filter");
		enterText("Search Filter", temp.get("SubAcctPrefix"));
		Thread.sleep(5000);
		clickElement("CLICKVA");
		aStatus = getText("GetStatus");
		Assert.assertTrue(aStatus.contentEquals(sStatus));
		temp.put("contextid", getText("GetContextID"));

	}

	@Then("^I verify Context ID Issuance Request is in \"([^\"]*)\" status$")
	public void i_verify_Context_ID_Issuance_Request_is_in_status(String sStatus) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		String aStatus;
		Thread.sleep(10000);
		clickElement("VA Structure PhyAcct Filter");
		enterText("Search Filter", temp.get("PhyAcct"));
		Thread.sleep(5000);
		clickElement("CLICKVA");
		Thread.sleep(5000);
		clickElement("VA Structure Product Filter");
		enterText("Search Filter", temp.get("Product"));
		Thread.sleep(5000);
		clickElement("CLICKVA");
		aStatus = getText("GetStatus");
		Assert.assertTrue(aStatus.contentEquals(sStatus));
	}

	@Then("^I approve the Context ID Issuance Request$")
	public void i_approve_the_context_id_issuance_request() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(5000);
		javaScriptClickElement("VA Structure CheckBox");
		clickElement("ApproveVA");
		clickElement("APPROVE Remarks");
		Thread.sleep(5000);
		clickElement("ClearFilters");

	}
	
	
	@When("^I add Sub Account Prefix for \"([^\"]*)\" to verify validation messages$")
	public void i_add_Sub_Account_Prefix_for_to_verify_validation_messages(String sVirtualAccountData) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		
		this.sVirtualAccountData = sVirtualAccountData;
		temp = CSAutomationHooks.virtualaccountJSONData.get(this.sVirtualAccountData);
		Map<String, String> vaContent = vamExcelData.get(sVirtualAccountData);
		// temp.put("Product", vaContent.get("Product"));
		clickElement("Add VA Structure");
		customer = vaContent.get("CUSTOMER");
		phyAcct = vaContent.get("PHYACCT");
		vaproduct = vaContent.get("PRODUCT");  
		VASTRUCT =vaContent.get("VASTRUCT");  
		VASTRUCTLEN = vaContent.get("VASTRUCTLEN"); 
		ISCONTEXT = vaContent.get("ISCONTEXT"); 
		CONTEXTTYPE = vaContent.get("CONTEXTTYPE"); 
		CONTEXTLENGTH = vaContent.get("CONTEXTLENGTH"); 
	}
	
	@Then("^I verify field validations for Customer field in SubAccount Prefix$")
	public void i_verify_field_validations_for_Customer_field_in_SubAccount_Prefix() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		FieldValidations fv = new FieldValidations(testContext);
		String[] valmsg = {"BLANK"};
		fv.validation(valmsg,"VNUM",34,"Customer","SUBACCTPREFIX");
		clickElement("VASAVE");
		Assert.assertEquals(getText("ALERTDIALOG"),fv.getErrorMessage("SUBACCTPREFIX", "Customer", "ALERTPOPUP"));
		
	}

	@Then("^I verify field validations for Physical Account field in SubAccount Prefix$")
	public void i_verify_field_validations_for_Physical_Account_field_in_SubAccount_Prefix() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		enterText("Customer",customer );
		waitForListVisible();
		clickElement("LIST");
		
		FieldValidations fv = new FieldValidations(testContext);
		String[] valmsg = {"BLANK"};
		fv.validation(valmsg,"VNUM",34,"Physical Account","SUBACCTPREFIX");
		clickElement("VASAVE");
		Assert.assertEquals(getText("ALERTDIALOG"),fv.getErrorMessage("SUBACCTPREFIX", "Physical Account", "ALERTPOPUP"));
		
	}

	@Then("^I verify field validations for Product field in SubAccount Prefix$")
	public void i_verify_field_validations_for_Product_field_in_SubAccount_Prefix() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		enterText("Customer",customer );
		waitForListVisible();
		clickElement("LIST");
		
		enterText("Physical Account",phyAcct );
		waitForListVisible();
		clickElement("LIST");
		
		FieldValidations fv = new FieldValidations(testContext);
		clickElement("VASAVE");
		Assert.assertEquals(getText("ALERTDIALOG"),fv.getErrorMessage("SUBACCTPREFIX", "VAProduct", "ALERTPOPUP"));
		String[] valmsg = {"BLANK"};
		fv.validation(valmsg,"VNUM",34,"VAProduct","SUBACCTPREFIX");
		
	}
	
	@Then("^I verify field validations for VA Structure field in SubAccount Prefix$")
	public void i_verify_field_validations_for_VA_Structure_field_in_SubAccount_Prefix() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		
		enterText("Customer",customer );
		waitForListVisible();
		clickElement("LIST");
		
		enterText("Physical Account",phyAcct );
		waitForListVisible();
		clickElement("LIST");

		enterText("VAProduct",vaproduct );
		waitForListVisible();
		clickElement("LIST");
		
		if (VASTRUCT.equals("Manual")) {
			
			FieldValidations fv = new FieldValidations(testContext);
			String[] valmsg = {"BLANK","VCHARLW","VCHARUP","VALFHA","VNUM",};
			fv.validation(valmsg,"VNUM",34,"Enter VA Structure","SUBACCTPREFIX");
			try {
				int len = Integer.parseInt(VASTRUCTLEN);
				len = len+3; 
				VASTRUCTLEN = String.valueOf(len);
			}catch(Exception e)
			{
				System.out.println(e);
			}
			
			enterText("Enter VA Structure",KeyGenerator.getRandomInteger(Integer.parseInt(VASTRUCTLEN)));
			clickElement("VASUBMIT");
			Assert.assertEquals(getText("ALERTDIALOG"),fv.getErrorMessage("SUBACCTPREFIX", "Enter VA Structure", "ALERTPOPUP"));

		}
	}
	

	@Then("^I verify field validations for Context ID Length field in SubAccount Prefix$")
	public void i_verify_field_validations_for_Context_ID_Length_field_in_SubAccount_Prefix() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		
		enterText("Customer",customer );
		waitForListVisible();
		clickElement("LIST");
		
		enterText("Physical Account",phyAcct );
		waitForListVisible();
		clickElement("LIST");

		enterText("VAProduct",vaproduct );
		waitForListVisible();
		clickElement("LIST");
		
		if (VASTRUCT.equals("Manual")) {
			
			enterText("Enter VA Structure",
					KeyGenerator.getRandomInteger(Integer.parseInt(VASTRUCTLEN)));

		}
		
		if (ISCONTEXT.equals("Yes")) {
			javaScriptClickElement("ContextIDY");
			Thread.sleep(10000);
			if (CONTEXTTYPE.equals("Manual")) {
				javaScriptClickElement("ContextIDIssuanceType");
			}
			
			FieldValidations fv = new FieldValidations(testContext);
			String[] valmsg = {"BLANK","VCHARLW","VCHARUP","VALFHA"};
			fv.validation(valmsg,"VNUM",34,"ContextID Length","SUBACCTPREFIX");
			
		//	CONTEXTLENGTH
			try {
				int len = Integer.parseInt(CONTEXTLENGTH);
				len = len+3; 
				CONTEXTLENGTH = String.valueOf(len);
			}catch(Exception e)
			{
				System.out.println(e);
			}
			
			enterText("ContextID Length",KeyGenerator.getRandomInteger(Integer.parseInt(CONTEXTLENGTH)));
			clickElement("VASAVE");
			Assert.assertEquals(getText("ALERTDIALOG"),fv.getErrorMessage("SUBACCTPREFIX", "ContextID Length", "ALERTPOPUP"));
			//System.out.println(getText("ALERTDIALOG"));
		}
	}
	
	@When("^I add Sub Account Issuance Request for \"([^\"]*)\" to verify validation messages$")
	public void i_add_Sub_Account_Issuance_Request_for_to_verify_validation_messages(String sVirtualAccountData) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		this.sVirtualAccountData = sVirtualAccountData;
		temp = CSAutomationHooks.virtualaccountJSONData.get(this.sVirtualAccountData);
		Map<String, String> vaContent = vamExcelData.get(sVirtualAccountData);
		clickElement("Add VA Issuance Request");
		
		customer = temp.get("Customer");
		phyAcct = temp.get("PhyAcct");
		VASTRUCT = vaContent.get("VASTRUCT");
		subprefix = temp.get("SubAcctPrefix");
		VASTRUCTLEN = vaContent.get("VASTRUCTLEN");
		VASUBACCTLEN = vaContent.get("VASUBACCTLEN");
		
	}
	
	
	@Then("^I verify field validations for Customer field in SubAccount Issuance Request$")
	public void i_verify_field_validations_for_Customer_field_in_SubAccount_Issuance_Request() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
		FieldValidations fv = new FieldValidations(testContext);
		String[] valmsg = {"BLANK"};
		fv.validation(valmsg,"VNUM",34,"Customer","SUBACCREQ");
		
		clickElement("VASAVE");
		Assert.assertEquals(getText("ALERTDIALOG"),fv.getErrorMessage("SUBACCREQ", "Customer", "ALERTPOPUP"));
		//System.out.println(getText("ALERTDIALOG"));
	}
	
	@Then("^I verify field validations for Physical Account field in SubAccount Issuance Request$")
	public void i_verify_field_validations_for_Physical_Account_field_in_SubAccount_Issuance_Request() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
		enterText("Customer",customer);
		waitForListVisible();
		clickElement("LIST");
		
		FieldValidations fv = new FieldValidations(testContext);
		String[] valmsg = {"BLANK"};
		fv.validation(valmsg,"VNUM",34,"Physical Account","SUBACCREQ");
		
		clickElement("VASAVE");
		Assert.assertEquals(getText("ALERTDIALOG"),fv.getErrorMessage("SUBACCREQ", "Physical Account", "ALERTPOPUP"));
		//System.out.println(getText("ALERTDIALOG"));
	}
	
	@Then("^I verify field validations for VA Structure field in SubAccount Issuance Request$")
	public void i_verify_field_validations_for_VA_Structure_field_in_SubAccount_Issuance_Request() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
		enterText("Customer",customer);
		waitForListVisible();
		clickElement("LIST");
		
		
		enterText("Physical Account",phyAcct);
		waitForListVisible();
		clickElement("LIST");
		
		FieldValidations fv = new FieldValidations(testContext);
		String[] valmsg = {"BLANK"};
		fv.validation(valmsg,"VNUM",34,"VA Structure","SUBACCREQ");
		
		clickElement("VASAVE");
		Assert.assertEquals(getText("ALERTDIALOG"),fv.getErrorMessage("SUBACCREQ", "VA Structure", "ALERTPOPUP"));
		//System.out.println(getText("ALERTDIALOG"));
	}
	
	@Then("^I verify field validations for SubAccount Number field in SubAccount Issuance Request$")
	public void i_verify_field_validations_for_SubAccount_Number_field_in_SubAccount_Issuance_Request() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
		enterText("Customer",customer);
		waitForListVisible();
		clickElement("LIST");
		
		
		enterText("Physical Account",phyAcct);
		waitForListVisible();
		clickElement("LIST");
		
		enterText("VA Structure", temp.get("SubAcctPrefix"));
		waitForListVisible();
		clickElement("LIST");
		
		if (VASTRUCT.equals("Manual"))
		{
			FieldValidations fv = new FieldValidations(testContext);
			String[] valmsg = {"BLANK"};
			fv.validation(valmsg,"VNUM",34,"Enter Sub Account Number","SUBACCREQ");
			
			//VASTRUCTLEN
			try {
				int len = Integer.parseInt(VASUBACCTLEN);
				len = len+1; 
				VASUBACCTLEN = String.valueOf(len);
			}catch(Exception e)
			{
				System.out.println(e);
			}
			
			enterText("Enter Sub Account Number",KeyGenerator.getRandomInteger(Integer.parseInt(VASTRUCTLEN)));
			clickElement("VASAVE");
			System.out.println(getText("ALERTDIALOG"));
			Assert.assertEquals(getText("ALERTDIALOG"),fv.getErrorMessage("SUBACCREQ", "Enter Sub Account Number", "ALERTPOPUP"));
		}
		else
		{
			FieldValidations fv = new FieldValidations(testContext);
			String[] valmsg = {"BLANK","VCHARLW","VCHARUP","VALFHA"};
			fv.validation(valmsg,"VNUM",34,"No.of Accounts","SUBACCREQ");
			
			enterText("No.of Accounts","0");
			clickElement("VASAVE");
			Assert.assertEquals(getText("ALERTDIALOG"),fv.getErrorMessage("SUBACCREQ", "No.of Accounts", "ALERTPOPUP"));
			System.out.println(getText("ALERTDIALOG"));
		}
	}
	
	@Then("^I verify field validations for Account Owner Name field in SubAccount Issuance Request$")
	public void i_verify_field_validations_for_Account_Owner_Name_field_in_SubAccount_Issuance_Request() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
		enterText("Customer",customer);
		waitForListVisible();
		clickElement("LIST");
		
		
		enterText("Physical Account",phyAcct);
		waitForListVisible();
		clickElement("LIST");
		
		enterText("VA Structure", temp.get("SubAcctPrefix"));
		waitForListVisible();
		clickElement("LIST");
		
		enterText("Enter Sub Account Number",KeyGenerator.getRandomInteger(Integer.parseInt(VASUBACCTLEN)));
		
		
		FieldValidations fv = new FieldValidations(testContext);
		String[] valmsg = {"BLANK","VCHARLW","VCHARUP","VALFHA","VNUM"};
		fv.validation(valmsg,"VNUM",201,"Enter Account Owner Name","SUBACCREQ");
		clickElement("CLICKVA");
		clickElement("VASAVE");
		Assert.assertEquals(getText("ALERTDIALOG"),fv.getErrorMessage("SUBACCREQ", "Enter Account Owner Name", "ALERTPOPUP"));
		//System.out.println(getText("ALERTDIALOG"));
		
		
	}

	
	@Then("^I verify field validations for Phone Number field in SubAccount Issuance Request$")
	public void i_verify_field_validations_for_Phone_Number_field_in_SubAccount_Issuance_Request() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
		enterText("Customer",customer);
		waitForListVisible();
		clickElement("LIST");
		
		
		enterText("Physical Account",phyAcct);
		waitForListVisible();
		clickElement("LIST");
		
		enterText("VA Structure", temp.get("SubAcctPrefix"));
		waitForListVisible();
		clickElement("LIST");
		
		enterText("Enter Sub Account Number",KeyGenerator.getRandomInteger(Integer.parseInt(VASUBACCTLEN)));
		
		enterText("Enter Account Owner Name", "AUTO" + KeyGenerator.getDate("CurrentDate", "ddMMyyyyHHmmss"));
		
		FieldValidations fv = new FieldValidations(testContext);
		String[] valmsg = {"VCHARLW","VCHARUP","VALFHA","VNUM"};
		fv.validation(valmsg,"VNUM",20,"PhoneNumber","SUBACCREQ");
		clickElement("VASAVE");
		Assert.assertEquals(getText("ALERTDIALOG"),fv.getErrorMessage("SUBACCREQ", "PhoneNumber", "ALERTPOPUP"));
		//System.out.println(getText("ALERTDIALOG"));
		
		
	}
	
	
	@Then("^I verify field validations for EmailID field in SubAccount Issuance Request$")
	public void i_verify_field_validations_for_EmailID_field_in_SubAccount_Issuance_Request() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
		enterText("Customer",customer);
		waitForListVisible();
		clickElement("LIST");
		
		
		enterText("Physical Account",phyAcct);
		waitForListVisible();
		clickElement("LIST");
		
		enterText("VA Structure", temp.get("SubAcctPrefix"));
		waitForListVisible();
		clickElement("LIST");
		
		enterText("Enter Sub Account Number",KeyGenerator.getRandomInteger(Integer.parseInt(VASUBACCTLEN)));
		
		enterText("Enter Account Owner Name", "AUTO" + KeyGenerator.getDate("CurrentDate", "ddMMyyyyHHmmss"));
		
		enterText("PhoneNumber",KeyGenerator.getRandomInteger(20));
		
		FieldValidations fv = new FieldValidations(testContext);
		String[] valmsg = {"EMAIL"};
		fv.validation(valmsg,"VNUM",20,"EmailID","SUBACCREQ");
		clickElement("VASAVE");
		Assert.assertEquals(getText("ALERTDIALOG"),fv.getErrorMessage("SUBACCREQ", "EmailID", "ALERTPOPUP"));
		//System.out.println(getText("ALERTDIALOG"));
		
	}
	
	@Then("^I verify field validations for PinCode field in SubAccount Issuance Request$")
	public void i_verify_field_validations_for_PinCode_field_in_SubAccount_Issuance_Request() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
		enterText("Customer",customer);
		waitForListVisible();
		clickElement("LIST");
		
		
		enterText("Physical Account",phyAcct);
		waitForListVisible();
		clickElement("LIST");
		
		enterText("VA Structure", temp.get("SubAcctPrefix"));
		waitForListVisible();
		clickElement("LIST");
		
		enterText("Enter Sub Account Number",KeyGenerator.getRandomInteger(Integer.parseInt(VASUBACCTLEN)));
		
		enterText("Enter Account Owner Name", "AUTO" + KeyGenerator.getDate("CurrentDate", "ddMMyyyyHHmmss"));
		
		enterText("PhoneNumber",KeyGenerator.getRandomInteger(20));
		
		enterText("EmailID","email.email@domain.com");
		
		enterText("Country","Singapore");
		clickElement("LIST");
		clickElement("PinCode");
		FieldValidations fv = new FieldValidations(testContext);
		String[] valmsg = {"VNUM","VCHARLW","VCHARUP","VALFHA"};
		fv.validation(valmsg,"VNUM",21,"PinCode","SUBACCREQ");	
	}
	
	
	@Then("^I verify field validations for Address1 field in SubAccount Issuance Request$")
	public void i_verify_field_validations_for_Address1_field_in_SubAccount_Issuance_Request() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
		enterText("Customer",customer);
		waitForListVisible();
		clickElement("LIST");
		
		
		enterText("Physical Account",phyAcct);
		waitForListVisible();
		clickElement("LIST");
		
		enterText("VA Structure", temp.get("SubAcctPrefix"));
		waitForListVisible();
		clickElement("LIST");
		
		enterText("Enter Sub Account Number",KeyGenerator.getRandomInteger(Integer.parseInt(VASUBACCTLEN)));
		
		enterText("Enter Account Owner Name", "AUTO" + KeyGenerator.getDate("CurrentDate", "ddMMyyyyHHmmss"));
		
		enterText("PhoneNumber",KeyGenerator.getRandomInteger(20));
		
		enterText("EmailID","email.email@domain.com");
		
		enterText("Country","Singapore");
		clickElement("LIST");
		
		enterText("PinCode","2293994");	
		
		clickElement("Address1");
		FieldValidations fv = new FieldValidations(testContext);
		String[] valmsg = {"BLANK","VNUM","VCHARLW","VCHARUP","VALFHA"};
		fv.validation(valmsg,"VNUM",36,"Address1","SUBACCREQ");	
		clickElement("VASAVE");
		Assert.assertEquals(getText("ALERTDIALOG"),fv.getErrorMessage("SUBACCREQ", "Address1", "ALERTPOPUP"));
		//System.out.println(getText("ALERTDIALOG"));
	}
	
	@Then("^I verify field validations for Address2 field in SubAccount Issuance Request$")
	public void i_verify_field_validations_for_Address2_field_in_SubAccount_Issuance_Request() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
		enterText("Customer",customer);
		waitForListVisible();
		clickElement("LIST");
		
		
		enterText("Physical Account",phyAcct);
		waitForListVisible();
		clickElement("LIST");
		
		enterText("VA Structure", temp.get("SubAcctPrefix"));
		waitForListVisible();
		clickElement("LIST");
		
		enterText("Enter Sub Account Number",KeyGenerator.getRandomInteger(Integer.parseInt(VASUBACCTLEN)));
		
		enterText("Enter Account Owner Name", "AUTO" + KeyGenerator.getDate("CurrentDate", "ddMMyyyyHHmmss"));
		
		enterText("PhoneNumber",KeyGenerator.getRandomInteger(20));
		
		enterText("EmailID","email.email@domain.com");
		
		enterText("Country","Singapore");
		clickElement("LIST");
		
		enterText("PinCode","2293994");	
		
		enterText("Address1","ADD1");	
		clickElement("Address2");
		FieldValidations fv = new FieldValidations(testContext);
		String[] valmsg = {"VNUM","VCHARLW","VCHARUP","VALFHA"};
		fv.validation(valmsg,"VNUM",36,"Address2","SUBACCREQ");	
		
	}
	
	@Then("^I verify field validations for Address3 field in SubAccount Issuance Request$")
	public void i_verify_field_validations_for_Address3_field_in_SubAccount_Issuance_Request() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
		enterText("Customer",customer);
		waitForListVisible();
		clickElement("LIST");
		
		
		enterText("Physical Account",phyAcct);
		waitForListVisible();
		clickElement("LIST");
		
		enterText("VA Structure", temp.get("SubAcctPrefix"));
		waitForListVisible();
		clickElement("LIST");
		
		enterText("Enter Sub Account Number",KeyGenerator.getRandomInteger(Integer.parseInt(VASUBACCTLEN)));
		
		enterText("Enter Account Owner Name", "AUTO" + KeyGenerator.getDate("CurrentDate", "ddMMyyyyHHmmss"));
		
		enterText("PhoneNumber",KeyGenerator.getRandomInteger(20));
		
		enterText("EmailID","email.email@domain.com");
		
		enterText("Country","Singapore");
		clickElement("LIST");
		
		enterText("PinCode","2293994");	
		
		enterText("Address1","ADD1");
		enterText("Address2","ADD2");
		clickElement("Address3");
		FieldValidations fv = new FieldValidations(testContext);
		String[] valmsg = {"VNUM","VCHARLW","VCHARUP","VALFHA"};
		fv.validation(valmsg,"VNUM",36,"Address3","SUBACCREQ");	
		
	}
	
	@Then("^I verify field validations for Address4 field in SubAccount Issuance Request$")
	public void i_verify_field_validations_for_Address4_field_in_SubAccount_Issuance_Request() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
		enterText("Customer",customer);
		waitForListVisible();
		clickElement("LIST");
		
		
		enterText("Physical Account",phyAcct);
		waitForListVisible();
		clickElement("LIST");
		
		enterText("VA Structure", temp.get("SubAcctPrefix"));
		waitForListVisible();
		clickElement("LIST");
		
		enterText("Enter Sub Account Number",KeyGenerator.getRandomInteger(Integer.parseInt(VASUBACCTLEN)));
		
		enterText("Enter Account Owner Name", "AUTO" + KeyGenerator.getDate("CurrentDate", "ddMMyyyyHHmmss"));
		
		enterText("PhoneNumber",KeyGenerator.getRandomInteger(20));
		
		enterText("EmailID","email.email@domain.com");
		
		enterText("Country","Singapore");
		clickElement("LIST");
		
		enterText("PinCode","2293994");	
		
		enterText("Address1","ADD1");
		enterText("Address2","ADD2");
		enterText("Address3","ADD2");
		clickElement("Address4");
		FieldValidations fv = new FieldValidations(testContext);
		String[] valmsg = {"VNUM","VCHARLW","VCHARUP","VALFHA"};
		fv.validation(valmsg,"VNUM",36,"Address4","SUBACCREQ");	
		
	}

	
	@When("^I add Sub Account Prefix for \"([^\"]*)\" and save it$")
	public void i_add_Sub_Account_Prefix_for_and_save_it(String sVirtualAccountData) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		// Write code here that turns the phrase above into concrete actions
				this.sVirtualAccountData = sVirtualAccountData;
				temp = CSAutomationHooks.virtualaccountJSONData.get(this.sVirtualAccountData);
				Map<String, String> vaContent = vamExcelData.get(sVirtualAccountData);
				// temp.put("Product", vaContent.get("Product"));
				clickElement("Add VA Structure");
				enterText("Customer", vaContent.get("CUSTOMER"));
				waitForListVisible();
				clickElement("LIST");
				customer = vaContent.get("CUSTOMER");
				enterText("Physical Account", vaContent.get("PHYACCT"));
				waitForListVisible();
				clickElement("LIST");
				phyAcct = vaContent.get("PHYACCT");
				enterText("VAProduct", vaContent.get("PRODUCT"));
				waitForListVisible();
				clickElement("LIST");
				vaproduct = vaContent.get("PRODUCT");

				if (vaContent.get("VASTRUCT").equals("Manual")) {
					enterText("Enter VA Structure",
							KeyGenerator.getRandomInteger(Integer.parseInt(vaContent.get("VASTRUCTLEN"))));

				}

				if (vaContent.get("ISCONTEXT").equals("Yes")) {
					javaScriptClickElement("ContextIDY");
					Thread.sleep(10000);
					if (vaContent.get("CONTEXTTYPE").equals("Manual")) {
						javaScriptClickElement("ContextIDIssuanceType");
					}
					enterText("ContextID Length", vaContent.get("CONTEXTLENGTH"));
				}
			//	javaScriptClickElement("Interest Defination");
				Thread.sleep(10000);
				clickElement("VASAVE");
				Thread.sleep(5000);
				Assert.assertEquals(getText("ALERTDIALOG"),"The record is saved successfully as a draft");
	}
	
	@When("^I add Sub Account Issuance Request for \"([^\"]*)\" and save it$")
	public void i_add_Sub_Account_Issuance_Request_for_and_save_it(String sVirtualAccountData) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		this.sVirtualAccountData = sVirtualAccountData;
		temp = CSAutomationHooks.virtualaccountJSONData.get(this.sVirtualAccountData);
		Map<String, String> vaContent = vamExcelData.get(sVirtualAccountData);
		clickElement("Add VA Issuance Request");
		enterText("Customer", temp.get("Customer"));
		waitForListVisible();
		clickElement("LIST");

		enterText("Physical Account", temp.get("PhyAcct"));
		waitForListVisible();
		clickElement("LIST");

		enterText("VA Structure", temp.get("SubAcctPrefix"));
		waitForListVisible();
		clickElement("LIST");

		if (vaContent.get("VASTRUCT").equals("Manual")) {

			enterText("Enter Sub Account Number",
					KeyGenerator.getRandomInteger(Integer.parseInt(vaContent.get("VASUBACCTLEN"))));
			enterText("Enter Account Owner Name", "AUTO" + KeyGenerator.getDate("CurrentDate", "ddMMyyyyHHmmss"));

		} else {
			enterText("No.of Accounts", "1");
		}

		Thread.sleep(5000);
		clickElement("VASAVE");
		Thread.sleep(5000);
		
		Assert.assertEquals(getText("ALERTDIALOG"),"The record is saved successfully as a draft");

	}
	
	@Then("^I discard the record and verify$")
	public void i_discard_the_record_and_verify() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		Thread.sleep(10000);
		javaScriptClickElement("VAMODIFY");
	    Thread.sleep(5000);
	    clickElement("VADISCARD");
	    System.out.println(getText("ALERTDIALOG"));
	    Assert.assertEquals(getText("ALERTDIALOG"),"The record is discarded successfully");
	}

}