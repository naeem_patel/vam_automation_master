package org.igtb.cs;

import org.junit.runner.RunWith;


import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@ExtendedCucumberOptions(
		jsonReport = "target/cucumber-report/cucumber.json",
		retryCount = 3,
		detailedReport = true,
		detailedAggregatedReport = true,
        overviewReport = true,
        coverageReport = true,
        jsonUsageReport = "target/cucumber-report/cucumber-usage.json",
        usageReport = true,
        toPDF = true,
        includeCoverageTags = {"chrome"},
        outputFolder = "target")

@CucumberOptions( 
		features = "src/test/resources/features",	
		glue ={"classpath:"},
		strict = true,
		monochrome = true,
		//tags ={"@Smoke1,@Smoke2,@Smoke3,@Smoke4"},
				// tags ={"@GTBIPSH-CS312,@GTBIPSH-CS313,@GTBIPSH-CS314"},
		//@GTBIPSH-CS518,@GTBIPSH-CS519,@GTBIPSH-CS520,@GTBIPSH-CS521,@GTBIPSH-CS522,@GTBIPSH-CS523,@GTBIPSH-CS524,@GTBIPSH-CS525,@GTBIPSH-CS526,@GTBIPSH-CS527,@GTBIPSH-CS528,@GTBIPSH-CS529
			tags ={"@CS-CSCRUD03"},
		plugin={"pretty","html:target/site/cucumber-pretty/html-report",
				"json:target/cucumber-report/cucumber.json","usage:target/cucumber-report/cucumber-usage.json","junit:target/cucumber-report/cucumber-results.xml",
				"rerun:target/rerun.txt"}//,"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"
		)
public class TestRunner {
}	