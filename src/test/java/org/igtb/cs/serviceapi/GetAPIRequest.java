package org.igtb.cs.serviceapi;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.igtb.itaf.ipsh.managers.FileReaderManager;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.internal.MethodHelper;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;



public class GetAPIRequest {
	
	
	public String GetAPIRequests(String bankEntityId,String url,String armorTicket,String param) throws Exception
	{
		
		
		
		//Specify Base URL
		RestAssured.baseURI = url;
		
		//Request Object
		RequestSpecification httpRequest = RestAssured.given();
		
		//Adding Header
		httpRequest.header("Content-Type","application/json");
		httpRequest.header("ARMOR_TICKET",armorTicket);
		
		//Response Object
		JSONObject requestParams = new JSONObject();
		httpRequest.body(requestParams.toJSONString());
		System.out.println(requestParams.toString());
		
		//Posting the Request
		Response response = httpRequest.request(Method.GET,bankEntityId);
		
		//Response Body
		String responseBody = response.getBody().asString();
		System.out.println("Response body is :"+responseBody);
		
		
		//Response Assertion
		String status = response.jsonPath().get("status");
		Assert.assertEquals(status, "Success");
		return status;
	}

}
